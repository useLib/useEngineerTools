/*****
 *
 * useEngineer is part of a SaaSS-Cloud-Service
 * %%
 * Copyright (C) 2021-22 Dr. Dirk Fischer, use-Optimierung, cologne
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *****/
window.useEngineer = ( function( D, W ) {
var
	copyright = "© Dr. Dirk Fischer, use-Optimierung, Cologne 2021; Version: 1.03"
	, homepage = "https://useengineer.com"
	, path = location.protocol == 'file:' ? homepage +'/'
	: location.href.slice( 0, location.href.lastIndexOf( '/' ) +1 )
	, emailDefault = 'testing@useengineer.com'
	/** BASIC SECTION *****************************************************************
		basic functions and shorteners (most from useLib)
	**********************************************************************************/
	, typeCheck = function( t, o ) {
		return Object.prototype.toString.call( o ) == '[object ' + t + ']';
	}
	, DOM = {
		isTouch: !!( W.DocumentTouch && D instanceof DocumentTouch )
		|| !!( 'ontouchstart' in W )
		|| ( !!( 'onmsgesturechange' in W ) && !!W.navigator.maxTouchPoints )
		, getCoord: function( ev, r ) {
			//ev = ev.touches ? ev.touches[ 0 ] : ev;
			r = r || {};
			if ( ev ) {
				r.x = ev.pageX;
				r.y = ev.pageY;
			}
			return r;
		}
		, getRect: function ( e ) {
		//@ gets structure {x ,y ,w, h} of pixel coordinates and dimensions of element
		var
			r = { x: 0, y: 0, w: 0, h: 0 }
			, rr
			;
			if ( e ) {
				rr = e.getBoundingClientRect();
				r.x = Math.round( rr.left );
				r.y = Math.round( rr.top );
				r.w = e.offsetWidth;
				r.h = e.offsetHeight;
			}
			return r;
		}
		, scrollIntoView: function ( e, delay, pos ) {
		//@ PARAM: elementToView, delayOfSecondsToStart [0], position 
		//@ [nearestIfNeeded, -1 == center, 1 == start, 'top', 'bottom' ] 
		//@ scrolls elements into view. Options not working in safari and IE =>
		//@ have to figger out if bottom or top (center == top).
		//@ REMARK: in some cases center is better i. e. for fastFeeder
			setTimeout( function() {
			var
				ops = /MSIE|Trident|(^((?!chrome|android|crios|fxios).)*safari)/i.test(
					navigator.userAgent )
				, r = !!e ? e.getBoundingClientRect() : 0	// outside screen?
				;
				if ( e == D.body ) {
					pos = pos == 'top' ? 0 : pos == 'bottom' ? D.body.scrollHeight
					: $I( pos );
					if ( ops )
						D.body.scrollTo( 0, pos );
					else
						D.body.scrollTo({ top: pos, left: 0, behavior: 'smooth' });
				}
				else if ( !!e && r.top < 0 || r.bottom > W.innerHeight ) {
					e.scrollIntoView( ops ? pos != 'bottom' || !!pos || r.top < 0
						: { behavior: "smooth", block: pos == 1 || pos == 'top'
						? 'start' : pos == 'bottom' ? 'end' : pos == -1
						? "center" : "nearest" }
					);
				}
			}
			, $I(( delay || 0 ) * 1000 ));
		}
		, remove: function( e ) {
			if ( e && e.parentNode )
				e.parentNode.removeChild( e );
		}
		, insertBefore: function( e, newNode ) {
			if ( e && newNode )
				e.parentNode.insertBefore( newNode, e );
			return newNode;
		}
		, insertAfter: function( e, newNode ) {
			if ( e && newNode ) {
				if ( e.nextElementSibling )
					e.parentNode.insertBefore( newNode, e.nextElementSibling );
				else
					e.parentNode.appendChild( newNode );
			}
			return newNode;
		}
		, draggable: function( e ) {
			e.style.position = 'absolute';
			e.addEventListener( _event( 'mousedown' ), function _dragIt( ev ) {
			var
				c = DOM.getCoord( ev )
				, r = DOM.getRect( e )
				;
				c.x -= r.x;
				c.y -= r.y;
				e.style.transform = 'none';
				_set( e, r.x, r.y );
				W.addEventListener( _event( 'mousemove' ), __moving );
				W.addEventListener( _event( 'mouseup' ), __reset );

				function __moving( ev ) {
				var
					_c = DOM.getCoord( ev )
					, tN = D.activeElement ? D.activeElement.tagName : ''
					;
					if ( tN != 'INPUT' && tN != 'TEXTAREA')
						_set( e, _c.x - c.x, _c.y - c.y );
				}
				function __reset() {
					DOM.activeTouch = false;
					W.removeEventListener( _event( 'mousemove' ), __moving );
					W.removeEventListener( _event( 'mouseup' ), __reset );
				}
			});

			function _set( e, x, y ) {
				e.style.left = x + 'px';
				e.style.top = y + 'px';
			}
			function _event( t ) {
				return !DOM.isTouch ? t : t == 'mousedown' ? 'touchmove'
				: t == 'mouseup' ? 'touchend' : 'touchmove';
			}
		}
		, posTouch: {}
		, ghostTouch: false
		, draggTouch: function( ev ) {	// create a ghost on dragg and move it...
		var
			c = DOM.getCoord( ev )
			;
			if ( ev.touches && ev.touches.length == 1 ) {
				ev.preventDefault();
				if ( !this.ghostTouch ) {
				var
					e = ev.currentTarget
					, rect = DOM.getRect( e )
					, ghost = e.cloneNode( true )
					;
					e.dispatchEvent( new Event( 'dragstart' ) );
					ghost.style = "position:absolute;display:block;z-index:199;opacity:0.5;";
					ghost.id += 'Ghost';					// prevent double id!
					D.body.appendChild( ghost );
					this.ghostTouch = ghost;
					this.posTouch.x = rect.x - c.x + W.visualViewport.offsetLeft;
					this.posTouch.y = rect.y - c.y + W.visualViewport.offsetTop;
					_set( ghost, this.posTouch );
					W.addEventListener( 'touchend', _touchReset );
				}
				else
					_set( this.ghostTouch, c );
			}			

			function _set( _e, _c ) {
				_e.style.left = Math.ceil( _c.x + DOM.posTouch.x ) + 'px';
				_e.style.top = Math.ceil( _c.y + DOM.posTouch.y ) + 'px';
			}
			function _touchReset( ev ) {
			// needed to figure out, where element is dropped and tell it drop function
			// REMARK: a better solution is not working on >ios12 (incredible good work?!):
			// e = [].slice.call( D.querySelectorAll( '[ondrop]:hover' )).pop()
			var
				e = DOM.getCoord( ev )
				;
				DOM.remove( DOM.ghostTouch );
				DOM.ghostTouch = false;
				e = D.elementFromPoint(
					e.x - W.visualViewport.offsetLeft
					, e.y - W.visualViewport.offsetTop
				);
				while ( e && !$A( e, 'ondrop' )) e = e.parentNode;
				if ( e ) { 
					//new Event targets are needed... (just a bit tricky ;-)
					Object.defineProperty( ev, 'target', { value: e });
					Object.defineProperty( ev, 'currentTarget', { value: e });
					e.ondrop.call( this, ev );
				}
				W.removeEventListener( 'touchend', _touchReset );
			}
		}
	}
	/*@ some short'n local functions incl. checks against failures etc.
	$  = getElementById
	$I = parseInt
	$A = manage Attributes := -1 = remove
	$CN = classList
	*/
	, $ = function( s ) {						// instead of getElementById
		return typeCheck( 'String', s ) && s.trim().length ? D.getElementById( s ) : false;
	}
	, $I = function( v ) { return parseInt( v, 10 ) || 0; }// make sure it's a integer
	, $A = function( e, n, v ) {				// setAttribute :: including remove
	//@ PARAM element, attributeName, attributeValue := if -1 remove else if empty get
		if (!!e && !!n && !!e.getAttribute ) {
			if ( n == "innerHTML" )
				n = e.innerHTML = v;
			else if ( v === -1 )
				n = e.removeAttribute( n );
			else if ( typeof v == 'string' )
				n = e.setAttribute( n, v );
			else {
				e = e.getAttribute( n ); // returns null or '' if empty or not defined
				n = typeof e == 'string' ? ( /value|data/.test( n ) || e.length
				? e : /selected|checked|disabled/.test( n ) ? n : false )
				: false;
			}
			return n;
		}
		return null;
	}
	, $CN = {
	//@ className :: there's a lot to do with className
		contains: function( e, n ) {   // check if className is set in element e
			return !e || !n || !e.classList ? false : e.classList.contains( n );
		}
		, add: function( e, n ) {     // adds className, if not already there
			return !e || !n || !e.classList ? false : e.classList.add( n );
		}
		, remove: function( e, n ) {  // removes className from element e
			return !e || !n || !e.classList ? false : e.classList.remove( n );
		}
		, toggle: function( e, n ) { // flips className from element e
			return !e || !n || !e.classList ? false : e.classList.toggle( n );
		}
		, replace: function( e, o, n ) { // replaces className o with n in element e
			return !e || !n || !e.classList ? false : e.classList.replace( o, n );
		}
	}
	, lastTime = new Date( 0 ).toUTCString()
	, lastLength = 0
	, lastMessage = false
   , hasCORSAPI = false
   , queue = []  // On some touch devices a lot interactions may be fired, while data
   // are transmitted. Worker or CORS message events fired highly asynchron: so all
   // submit data stored in between and and send after a response while store is empty
	, ajax = function( method, url, postdata=null, fn, time, APPENDED=false ) {
	// here is used a special programming technique. An iframe is used like a worker
	// of a Progressive Web App. At least its quite similar, but will also work with
	// older browser versions, that may still remain on some workplaces for
	// compatibility or other production reasons. It may be a new software shall be
	// created by support and use of useEngineers tools.
	var
		js = JSON.stringify({
      method: 'post'
      , url: url
      , postdata: postdata
      , timeout: time || 3500
      , lastTime: lastTime
      , lastLength: lastLength
      , APPENDED: APPENDED
   	})
   	;
   	if ( !queue[ 0 ] || queue[ 0 ].js != js )
   		queue.unshift({ js: js, fn: fn });

		ajax.cllbck = function( h ) {
		var
			q = queue.pop()
			;
			$CN.toggle( $( "groupMode" ), 'active' );
			if ( h.status >= 500 ) {
				if ( h.status == 500 )
					createDialog( 'error', msg.errorServer );
			}
			else if ( h.status >= 400 )
				q.fn( false );
			else if ( h.status == 304 && APPENDED )
				q.fn( -1 );
			else if ( h.status == 200 || h.status == 202 || h.status == 206 ) {
				lastTime = h.lastTime;
				lastLength = $I( h.lastLength )
				+ ( h.status == 206 && APPENDED ? lastLength : 0 );
				q.fn( h.responseText, h.status == 206 );
			}
			if ( queue.length )
	   		_tellPostAPI( queue[ queue.length -1 ].js );
		}
		;
		if ( queue.length == 1 )
	   	_tellPostAPI( queue[ 0 ].js );

      function _tellPostAPI( json ) {
      var
         e = $( 'useLibCORSAPI' )
         , url = path +'exchange/exchangeCorsAPI.html'
         ;
         if ( !e && !hasCORSAPI ) {
         var
            e = D.createElement( 'iframe' )
            ;
            e.id = 'useLibCORSAPI';
            e.src = url;
            e.style = "position:absolute;width:0;height:0;border:none;";
            ( D.documentElement || D.body ).appendChild( e );
            e.onload = function() {
            	_tellPostAPI( JSON.stringify({ method: 'check' }));
            };
            W.addEventListener( "message", __onMsg, false );
         }
         else // its caused on crossorigin and security a bit tricky...
            e.contentWindow.postMessage( json
            , location.origin == 'null' || location.origin == 'file://'
            ? '*' : location.origin );

         function __onMsg( ev ) {
            if ( ev.origin == location.origin
            || (( location.origin == 'null' || location.origin == 'file://' ) 
            && path.indexOf( ev.origin ) > -1 )) {
     				ev.preventDefault();
					ev.stopPropagation();
               if ( ev.data == 'active' ) {
                  hasCORSAPI = true;
                  _tellPostAPI( json );
               }
               else if ( ajax.cllbck && ( ev.data != lastMessage
               || !/status:20/.test( ev.data ))) {
               // unfortunately message is fired twice, it has to be stored to check!
		            try {	ajax.cllbck( JSON.parse( ev.data )); }
		            catch( e ) { ajax.cllbck( ev.data ); }
		            lastMessage = ev.data;
		         }
            }
            return true;
         }
      }
   }
   , str2url64 = function( s, count ) {
		s = btoa( s ).replace( /\+/g, '-' ).replace( /\//g, '_' ).replace( /=/g, '' );
		return !!count ? ( s +'0000000' ).slice( 0, count ) : s;
	}
   , url642str = function( s ) {
      return atob( s.replace( /-/g, '+' ).replace( /_/g, '/' ));
   }
   , escapeSurrogates = function( _s ) {
   //@ prevent high-low pair URIError: malformed URI sequence. Well, not known below...
		return _s.replace( /[\uD800-\uDFFF]/g, function( c ) {
			return '\\u'+ c.charCodeAt( 0 ).toString( 16 );
   	});
   }
   , unescapeSurrogates = function( _s ) {
		return _s.replace(/\\u([0-9A-Za-z]{4})/g, function( _, c ) {
			return String.fromCharCode( '0x' + c );
		}); 
   }
   , unicode2url64 = function( s, count ) {
	//https://stackoverflow.com/questions/30106476/using-javascripts-atob-to-decode-base64-doesnt-properly-decode-utf-8-strings
      return str2url64( encodeURIComponent( escapeSurrogates( s )
      ).replace( /%([0-9A-F]{2})/g, function( _, hex ) {
      	return String.fromCharCode( parseInt( hex, 16 ))
      }), count );
   }
   , url642unicode = function( s ) { // take modern, it's much faster
		return unescapeSurrogates( decodeURIComponent( url642str( s ).split( '' ).map(
			c => '%' + ('00' + c.charCodeAt( 0 ).toString( 16 )).slice( -2 )
		).join('')));
	}
   , delayRemove = function( e, cL, t ) {
	//@ PARAM: Element, classNamesFormTo, timeInMilliSeconds
	//@ adds a className for animation and removes it after the animation has finished.
		if ( !typeCheck( 'Array', cL ))
			cL = [ cL, 0 ];
		if ( !!e ) {
			$CN.add( e, cL[ 0 ]);
			t = $I( getComputedStyle( e, null ).getPropertyValue( 'animation-period' )
			|| t );
			setTimeout(( function( _e, _cL ) { // create closure, could be called multiple
				return function(){
					$CN.remove( _e, _cL[ 0 ]);
					$CN.remove( _e, _cL[ 1 ]);
				}
			})( e, cL ), t );
		}
	}
	, storeIt = function( s, fname, end ) { 
   var
      d = s.length > 4 ? D.createElement( 'a' ) : false
		;
		if ( d ) {
			fname = fname +'_'+ _getLocalTimeStr() + language +'.'+ end;
			end = end == "html" ? "text/html" : end == "xls"
			? "application/vnd.ms-excel"
			// "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet"
			: end == "json" ? "application/json" : "text/html";
			d.href = "data:"+ end +";charset=utf-8,"+ encodeURIComponent( s );
			d.download = fname;
			D.body.appendChild( d );
			d.click();
			setTimeout( function() { DOM.remove( d ); }, 0 );
		}
		return false;

	   function _getLocalTimeStr() {
	   var
	   	d = new Date()
	   	;
	   	d.setHours( d.getHours() - d.getTimezoneOffset() / 60 );
			return d.toISOString().substring( 0, 16 ).replace( /:/g, '' );   	
	   }
	}
	/** CRYPT SECTION *****************************************************************
	To secure the submitted data from reading of unauthorized third - even from server
	service - they are crypted via a pseudo onepad. The cryption will also also work
	on browser versions without window.crypto support! Therefore the password has to be
	given as array of 32 integer values.
		All data is transmitted and processed as UTF-8 string.
		Each string is an encrypted block.
		Each block starts with 16-bit block length, 16-bit mask type and the	encrypted 
		data protection is based on four secrets, known only to the group. Three are
		included in the invitation email encoded in the link:
			1.secret: path to the encrypted file (≈16 * 16 bit = 256 bit).
			2.secret: key base (32 * 16 bit = 512 bit)
			3.secret: prefix block length (9 bit)
			4.secret: mist of data on begin (0 - 128 16-bit-sign) [crypto random] just part of exchange file.			
		decryption and encryption
			block = length transmission string
			+ 16 bit random value
			+ xor concatenation of each UTF-8 character with the random string generated from key base and random value (pseudo onepad)
		Result = concatenation of all decrypted blocks.

		its a pseudo onepad because the base values are variated via a two times rotation with a random value witch is send as part of the block.
	***********************************************************************************/
   , getRandom16 = function( c ) {
   	if ( !!W.crypto && !!W.crypto.getRandomValues )
			return Object.values( W.crypto.getRandomValues( new Uint16Array( c )));
		else {		// if older browser version without crypto methods
			for ( var i = 0, a = []; i < c; i++ )
				a.push( Math.floor( Math.random() * 65535 ));
			return a;
		}
   }
	, getRandomUnicode = function( c ) {
		return getRandom16( c ).map( _c => String.fromCharCode( _c )).join( '' );
	}
	, cryptbase2str = function( c ) {
		return str2url64( 
			c.map( s => String.fromCharCode(( s >> 8 )& 0xff )
			+ String.fromCharCode( s & 0xff )).join( '' )
		);
	}
	, str2cryptbase = function( s ) {
		s = url642str( s );
		for ( var i = 0, b = []; i < s.length; i += 2 )
			b.push( parseInt( '0x'+ s.charCodeAt( i ).toString( 16 )
			+ ( '0'+ s.charCodeAt( i +1 ).toString( 16 )).slice( -2 )));
		return b;
	}
	, cryptIt = function( s, mask, hash, OUT ) {
	//@ just xor each char with cryptohash, but as blockcain... The hash result of the
	//@ previous block is always used as hash for the next one. When decrypting, the
	//@ previous hash value one must be stored between (OUT) to use it in the next step.
		hash = !!hash ? hash : data.cryptBase;
		if ( !!hash ) {
			hash = hash.map( c => c ^ mask );
			if ( OUT )
				OUT = hash.slice( 0 );
			for (var i = 0, l = s.length + hash.length, c = ''; i < l; i += hash.length ) {
				for (var j = 0; j < hash.length && ( i + j ) < s.length; j++ ) {
					hash[ j ] = ( s.charCodeAt( i + j ) ^ hash[ j ]) & 0xffff;
         		c += String.fromCharCode( hash[ j ]);
         		if ( OUT.length )
						OUT[ j ] ^= hash[ j ];
				}
				if ( OUT.length )
					hash = OUT.slice( 0 );
         }
         s = c;
      }
		return s;
	}
	, cryptIn = function( s, hash ) {
	var
		steps = Math.ceil( s.length / 65533 ) // 2^16 == 65536
		, mask = getRandom16( steps )
		, out = ''
		;
		for ( var i = 0, len; i < steps; i++ ) {
			len = s.length > 65533 ? 65535 : s.length +2
			out += String.fromCharCode( len ) + String.fromCharCode( mask[ i ])
			+ cryptIt( s.slice( 0, len -2 ), mask[ i ], hash, false );
			s = s.slice( len -2 );
		}
		return escapeSurrogates( out );
	}
	, cryptOut = function( s, hash ) {
	var
		out = ''
		, len
		;
		s = unescapeSurrogates( s );
		while( s.length ) {
			len = s.charCodeAt( 0 );
			out += cryptIt( s.slice( 2, len ), s.charCodeAt( 1 ), hash, true );
			s = s.slice( len );
		}
		return out;
	}
	, checkHash = function( pw ) {
		if ( Array.isArray( pw ))
			return pw;
		pw = pw.replace( /\s+/g, '' ).split( ',' );
		pw.pop();
		return pw.length == 32 && /^\d+$/.test( pw.join( '' )) ? pw : false;
	}
	, getHashes = function( cryptType, a, fn, i ) {
	//@ creates an array of different hashValues based on the given values, i. e. local,
	//@ global or fileName. Then it calls the given function
	var
		direct = checkHash( a[ 0 ] )
		;
		if ( direct )
			fn([ direct ]);
		else if ( !!W.crypto && !!W.crypto.subtle ) {
			i = i || 0;
			if ( !!a[ i ]) {
	   		crypto.subtle.digest( cryptType
	   		, new TextEncoder( "utf-8" ).encode( a[ i ])).then( function( hash ) {
	            hash = new DataView( hash );
					for ( var j = 0, s = ''; j < hash.byteLength
					; s += String.fromCharCode( hash.getUint8( j++ )));
					a[ i ] = s;
					getHashes( cryptType, a, fn, ++i );
				});
			}
			else
				fn( a );
		}
		else
			createDialog( 'error', msg.noCrypt );
	}
   , identLen = 88
	, initHash = false
	, testHash = false
	, emailSalt = ",區叹箛䡼䄚瑡頖荥鐺"
	, globalSalt = ",疻鰇槂Ⱚ锏ᥤ☶䇗䡍捾"	// don't forget one (!) old browser comma in front
	, cryptValidation = ",뤘ӗﾚ鵕饕ꨯ餥졊⮧"
	, groupSalt = ",ϡ뚥ᙱ퇀籋헌嵻욅"
	, testSalt = ",㍦ꋝ஬向䧼쉱船僁䰣"
	, joinCheck = {
		joins: []
		, myJoin: '0-0'
		, myId: '0'
		, getId: function() {
		var
			tries = 5000
			, id
			;
			do {
				id = String( Math.max( getRandom16( 1 ), 2 ));
			} while( joinCheck.joins.filter( a => a.indexOf( id ) === 0 ).length > 0
			&& --tries );
			joinCheck.myId = id;
			id += '-'+ getRandom16( 1 );
			joinCheck.myJoin = id;
			joinCheck.joins.push( id );
			return id;
		}
		, setId: function( id ) {
			if ( joinCheck.joins.indexOf( id ) < 0 )
				joinCheck.joins.push( id );
		}
		, checkId: function( id ) {
			if ( joinCheck.joins.filter( a => a.indexOf( id ) === 0 ).length !== 1 ) {
				id = joinCheck.getId();
				sendActs([ getAct( 'viewJoin', viewData, id )]);
				return false;
			}
			return true;
		}
		, getJoins: function() {
			return joinCheck.joins.map( id => getAct( 'viewJoin', viewData, id ));
		}
	}
	, checkPassword = function( e ) {
	var
		log2 = Math.log2 || function(x) { return Math.log(x) / Math.LN2; }	// polyfill
		;
		$A( e.parentNode, 'data', Math.floor( _calcQuality( e.value ) /5 ) *5 );

		function _calcQuality( p ) {
		//@ calculate bit entropie, the given parameter is the minimum entropie value.
		//@ REMARK: It's a rough approach, over five different sign types, elimination
		//@ 	of repetitions and standard pattern from alphabethical and keyboard order.
		var
			t = [ /[a-z]/g, /[A-Z]/g, /[_\.\*+:#!?%\|@\{\}\[\]\(\);=\“&$\\/,-]/g
			, /[0-9]/g, /[\u00C0-\u017F]/g ]
			, syllable = /(ABE|ACH|ALL|AND|ARE|AUF|AUS|BEN|BER|BUT|CHE|CHT|DAS|DEN|DER|DIE|EIN|EIT|END|ENT|ERA|ERE|ERS|ESE|EVE|FOR|GEN|HAD|HAT|HEN|HER|HIN|HIS|ICH|IGE|INE|ING|ION|IST|ITH|LIC|LLE|MEN|MIT|NDE|NEN|NGE|NIC|NOT|NTE|OME|OUL|OUR|REN|SCH|SEI|SEN|SHO|SIC|SIE|STE|TED|TEN|TER|THA|THE|THI|TIO|ULD|UND|UNG|VER|WAS|WIT|YOU|AL|AN|AR|AS|AT|AU|BE|CH|DA|DE|DI|EA|ED|EI|EL|EN|ER|ES|GE|HA|HE|HI|HT|IC|IE|IN|IS|IT|LE|LI|ME|ND|NE|NG|NT|ON|OR|OU|RE|SC|SE|SI|ST|TE|TH|TI|TO|UN|VE|WA)/ig
			, pp = p
			, c = 0         // count of used char amount
			;
			if ( !p.length )
				return 0;
			// first step: check different chartypes to calculate permutation base
			for ( var i = 0; i < t.length; i++ ) {
				if ( pp.match( t[ i ])) {
					c += i < 3 ? 26 : 10; 	// european chars are 201!, but not on keyboards
					p = p.replace( t[ i ], '' );
				}
			}
			if ( p.length )		// still length...
				c += 10;				// ...some very special chars
			p = pp;
			// second step: eliminate more than two repeated patterns.
			//  result => shorter string, that causes a calculation on secured side
			p = p.replace( /(.)\1\1+/g, '$1$1' );
			// third step: get length where standardPattern counted as one sign
			p = __replaceStandardPattern( p );
			// fourth step: eliminate frequent syllables to rate secure against dictionary hacks
			p = p.replace( syllable, '\t' );
			// normalize to 10 counting chars with a calced entropie value around 65 
			return Math.min( 99, parseInt( 80 / 65 * log2( c ) * p.length ));

			function __replaceStandardPattern( p ) {
			var
				t = '!"§$%&/()=?qwertzuiop+asdfghjkl#<yxcvbnm,.-'
				+ '1234567890abcdefghijklmnopqrstuvwxyz'
				;
				t += t.split('').reverse().join('');    /* also reverse */
				p = ___check( p.toLowerCase());
				return p;

				function ___check( s ) {
				//@ step through hole password and cut stepwise chars, if result
				//@ is part of test string, replace it with one char instead
					if ( s.length > 5 )
						for ( var i = 0; i < s.length - 4; i++ ) {
							for ( var j = 0; j <= i; j++ )
								if ( t.indexOf( s.slice( i - j, s.length -j )) > -1 ) {
									// replace just with tab
									return ___check( s.slice( 0, Math.max( i - j, 0 )))
										+ '\t' + ___check( s.slice( s.length - j ));
							}
						}
					return s;
				}
			}
		}
	}
	/** END OF CRYPT SECTION **********************************************************/ 
	, titleDefault = 'Online-%§% (also for groups)'
	, paypalDonationLink = "https://www.paypal.com/donate/?hosted_button_id=RN7YDBTDH4W58"
	, language = '.de'
	, msg = {
		helpText: `<h2>Control (above the supply area)</h2>
		<ul>
		<li>These <b>instructions</b> can be accessed by clicking on &#x2753; displayed.</li>
		<li>An <b>additional column</b> can be added using the \u27A1\uFE0F button above the template.</li>
		<li>The \uD83C\uDD95 button can be used to create an empty <b>further view</b>.</li>
		<li>The <b>size adjustment</b> of the display area is done with <span class="zoomin">&#x1F50D;</span> (reduce)</li>
		<li>and <b>vice versa</b> using <span class="zoomout">&#x1F50E;</span> (enlarge).</li>
		<li>With the \uD83D\uDCBB or %%</li>
		<li>The current status can be <b>saved</b> locally as a JSON file by clicking on the save button \uD83D\uDCBE in the controller.</li>
		<li>By clicking on the &#x1F4CB; button, the current work data can be saved locally as a prototype HTML APP in the file itself, so that it can be <b>sent by email</b>. This allows the current state to be tested directly in test mode.</li>
		<li>The work result can be exported by clicking on the Export button next to it >&#x1F4DD; be <b>exported</b> locally as wiki text.</li>
		<li>The <button id="privatework" style="font-size:0.7em;border:none;background:none;"></button> button opens a private work window, it is used for <b>preparing< /b> and <b>Try</b> as well as the local <b>import</b> of already existing data.</li><li>Use the &#x1F464; button <b>Enable moderation access</b > you get the <b>authorization</b> to start group work (sessions). An accounts at <a href="`
		+ homepage + `" target="_blank">useEngineer cloud service</a> also provides information on how <b>usability engineering</b> can be successfully implemented with the help of the <b>useEngineer app</b> tools An account or <b>account</b> can be opened there for unlimited work.</li>
		<li><b>Group links</b> can be created using the \uD83D\uDC65 button and the moderation password, and the exchange file can also be reorganized in the process.</li>
		<li>If you click on the &#x1F511; button, the group password is displayed <b>as a list of numbers</b> instead of entering a password in older Internet programs (browsers).</li>
		</ul>
		<p>So that all user actions can also be traced <b>barrier-free</b>, they are logged in an invisible list (anonymized). This can be read using a screen reader.</p>
		<p>If you want to create a PDF printout, don't forget to select landscape view and adjust the size if necessary.</p>
		<p>Basically, <b>locally saved content</b> can be <b>loaded</b> again using drag & drop. It is also possible to <b>add</b> data to the currently active ones and to <b>import</b> term lists. For this <b>expert functionality</b>, please refer to the <b>documentation</b>.</p>`
		, getInitPassword: [ `To check the group membership, the group password must 
			be entered (alternative for older technology: list of numbers).`, "Password" ]
		, noGroup: `<h2>No working group found</h2>
		<p>Unfortunately, no working group could be assigned to the transmitted data! The <b>lifetime</b> may have been exceeded. This group must first be recreated using the <b>moderation password</b>.</p>
		<p>The Network access is now disabled and it can only be <b>worked 
		locally</b>.</p>`
		, errorServer: `Unfortunately, the action could not be executed on the 
		server!</br>There may be an overload or some other fault has occurred. 
		Please try again later.`
		, errorLink: `Unfortunately, an error occurred while decrypting the group 
		link!</br>Either the link is outdated or the password is incorrect.`
		, errorTest: `Unfortunately, an error occurred while decrypting! </br>
		Possibly. Wrong password?`
		, newGroupMissing: "The group exchange must first be created again!"
		, privatework: [ `<h1>Open private working window</h1>
		<p>This window is used for private <b>preparing</b> and <b>trying out</b> as well as local <b>import</b> of existing data. The elements can then be transferred to the group or main work window using <b>drag & drop</b>.</p>
		<p>The elements <b>for all accessible </b> and visible. Of course you can also drag elements from the main window <b>into private</b>!</p>`
		, `<p>Since drag & drop between two windows is not possible on many 
		<b>touch surfaces</b>, the elements can be dropped onto the &#x1F4E1;-<b><i>drop 
		zone</i></b> to be <b>transferred</b> to the group work window.</p>`
		, `Private working window <span style="font-size:50%">Drag & Drop</span>`
		, `Drag & drop from private window to group work window`
		]
		, choiseEdit: `<h1>Please enter the selection options</h1>
		<p>In order to achieve an improved simulation, different selection types can be indicated. The following rules apply:</p>
		<ul>
		<li>Separate individual options with "," or a line break.</li>
		<li>Usually a select list is indicated.</li>
		<li>Two A switch appears (toggle).</li>
		<li>A preceding "\u2212" results in a radio list (single select). </li>
		<li>A preceding "+" results in a checkbox list (multiselect).</li>
		</ul>`
		, targetEdit: `<h1>Please enter a destination</h1>
		<p>To achieve an improved simulation, buttons, tabs and calling a modal dialog for additional input can be linked to other designs in the design list. The draft name can be entered here.</p>`
		, viewEdit: `<h1>Please enter a %&%sname</h1>
		<p>The %&%snames can be specified for buttons and tabs for linking to achieve an improved simulation.</p>`
		, editParallel: `<h1>Text changed in parallel</h1>
		<p>Text changed:</br>
		<i>%%</i></br>was edited externally at the same time as:</br>
		<i>$$< /i></p>`
		, editOverwrite: `<p>The parallel change can be overwritten with "confirm".</p>`
		, editTypeCopy: `<p>The element is copied with "confirm" and this change is also accessible to all group members.</p>`
		, deleteConfirm: `<h2>Delete confirmation required</h2>
		<p>There may be a lot of teamwork in %%, do you really want to delete?</p>`
		, dropJson: `Unfortunately, only JSON files are allowed via drag & drop!`
		, dropNetworking: `<h1>Imports are not possible during group work!</h1>
		<p>Please confirm that you are leaving the group work.</p>
		<p>Using the moderation password, you can reopen the <b>online group after the import </b> and overwrite the active data with the imported ones!</p>
		<p>You can also open a private working window, import it there and then transfer selected elements to the group or main working window.</p>`
		, dropImport: `Unfortunately, an error occurred while importing %%!`
		, initAccount: [ `<h2>Activate and confirm moderation access</h2>
		<p> With the moderation access you get the <b>authorization</b> to open <b>unlimited</b> group work (sessions). This requires a paid account with the 
		<a href=\"`+ homepage + `\" target=\"_blank\">useEngineer cloud service</a>. This authorization can also be given to you <b>by third parties</b> if they set up access for your e-mail address.</p>
		<p>Thanks to useEngineer moderation access, previously saved locally and Drop loaded JSON data can always be edited, even with an <b>\"old\" group link</b>.</p>
		<p>A confirmation can also be <b>repeated</b> in case of <i>Forgot moderation password!</i>. A previously transmitted moderation password is retained until the new one is confirmed. All data are processed <b>encrypted</b> (salted SHA hash values).</p>`
		, "Email address for moderation access"
		, "One-time verification code from email"
		, `New moderation password <a href="javascript:useEngineer.getPassword();">I forgot the moderation password!</a>`
		]
		, getPassword: [ `<h2>Forgot the moderation password!</h2>
		<p> If the moderation password has been forgotten, the moderation access must be <b>initiated again</b>. For this purpose, a <b><i>unique verification code</i></b> is generated and entered in the access verification. This code is sent to the e-mail address for moderation access.</p>
		<p>This procedure ensures that <b>no misleading links</b> can be used for password fishing. On the other hand, an <b>email can be ignored</b> caused on 'Forgot moderation password!' by <b>unauthorized third</b>.</p>`
		, "Email address for moderation access"
		]
		, addGroup: [ `<h2>Open online group work (session)</h2>
		<p> Basically, an account with useEngineer is required to open a group work. For this, the authorization is checked by means of email and password. JSON data previously stored locally and loaded using drag & drop are taken over.</p>
		<p>For test purposes, a session can also be opened with the email: <b>`
		+ emailDefault +`</b> without a moderation password . Such a test session ends automatically after about 20 minutes. A new group link may then have to be generated and sent with saved JSON data.</p>`
		, "Title or header"
		, "Email address for moderation access"
		, `Moderation password <a href="javascript:useEngineer.getPassword();">I forgot the moderation password!</a>`
		, "Create a new group link, also so that old links no longer work."
		, "Group password for data decryption<br>"
		, `<small>Without change, the previously assigned remains. You should then save this link change in the JSON file.</small>`
		]
		, reloadWait: `Waiting for other group accesses to be inactivated to avoid inconsistent data. `
		, newGroupLink: `The local JSON file should be resaved/overwritten because the encryption base is changed and is otherwise not stored anywhere.`
		, reloadData: `<h2>Attention Reload necessary!</h2>
		<p>The transfer file has just been overwritten. To keep all data uniform or consistent, all data will be reloaded.</p>.`
		, newPassword: `The new moderation password to the email address: <b>%%</b> has been entered and can be used to open online groups. The number of active groups is limited to five for security reasons.`
		, sendIdent: `An email with an active <i>one-time verification code</i> was successfully sent to <b>%%</b>!`
		, noAccount: `<h2>No access authorization</h2>
		<p>Unfortunately, there is no access authorization for the e-mail address: <b>%%</b>. It is necessary to create or unlock this using the <a href="`+ homepage
		+ `">useEngineer Cloud Service</a>. Without access authorization it is not allowed to set a new moderation password or to create an exchange file.
		<p>If you previously had an authorization, it may need to be extended. To do this, please contact the person that manages the cloud service.</p>`
		, noCryptData: `<h2>No information about the exchange file available</h2>
		<p>The currently active data does not contain any encryption data in the browser path, nor was a local JSON file loaded that contains this data. For the group exchange it is necessary to create an exchange file using the <b>useEngineer access data</b> and save the associated encryption data in a JSON file.</p>`
		, testingTime: `Attention, the test time for this online group work expires in about %% minutes!`
		, noAddGroup: `The password or encryption data is incomplete or incorrect. Unfortunately, the exchange file could not be created again.`
		, noCrypt: `<h2>Unfortunately an outdated browser version</h2>
		<p>This version of the browser does not support the necessary encryption technology. An update is highly recommended, also because other features may not work.</p>
		<p> Registered party members can access an alternative list of 32 numbers using the &#x1F511; button. These numbers can be entered in place of the password.</p>
		<p>If dictation may cause errors, this list can be sent using a secure group chat, for example.</p>`
		, numberCrypt: `<h2>Password replacement: list of numbers</h2>
		<p> If the browser of a group member is older, they may enter or copy the following list of numbers as a password instead:</br>%%</br>(So the encrypted password looks like a series of random numbers [32 * 16bit]).</p>
		<p>Older browser versions may still be installed on some workstations for compatibility or other production reasons.</p>`
		, noHash: `Unfortunately, no suitable or correct numerical list is available.`
		, emailSubject: '%§%: link to the group work!'
		, emailBody: `<p>Just click at the right time:</br>
		<a href="%%0">%%1</a></p><p>Lot of fun ;-)</br>
		Yours use-Optimierung (use optimizing)</p>`
		, emailDialog: `<h2>Send invitation</h2>
		<p>Clicking on the following link opens an email that can be sent to the group members. Depending on the security requirements, the group password can be sent directly or transmitted in another way:</p>`
		, recommendSubject: "%$%: a great %§%-tool!"
		, recommendBody: `<p>Hi,</br>I found something there that you can put to good use:</br><a href="%%0">%%1</a></p><p>Have fun ;-)</br></p>`
		, recommendDialog: `<h2>Support developers</h2>
		<p>To support further development and as recognition, the author would be happy to receive a freely selectable amount of money via PayPal donation:</p>
		<a class="donation" target="_blank" href="`+ paypalDonationLink +`"></a>
		<h2>Send recommendation</h2>
		<p>Clicking on the link opens an email that can be sent using your email program:</p>`
		, storeExport: "<h2>Export work data locally via \"confirm\"?</h2>"
		, noExport: "Without %§% content, an export unfortunately makes no sense!"
		, errorToolType: `<b>Caution:</b>This JSON data does not belong to the active useEngineering tool, but to the tool: %%!<br>
		The loading process is therefore aborted.`
		, localStore: [ `<h2>Save %$% as local Html app</h2>
		<p>You can save the file anywhere and then open it locally in the browser, e.g. by double-clicking. In addition, the currently active data can be stored within this file, so that it is always displayed when it is opened. All you have to do is check the box: <i>integrate active data</i>. If you also enter a password, the data will be stored encrypted using the password</p>
		<p><b>Caution:</b> Certain browser settings always save files in the Download folder without a choice!</p>`
		, "integrate active data"
		, "local passwort"
		]
		, localDecrypt: [ `<h2>Decrypt embedded data</h2>
		<p>This %$% application contains encrypted data. These can be decrypted by entering the <b>correct local password</b> and then be displayed.</p>`
		, "local passwort" ]
		, noSupport: `<h2>Browser was not tested!</h2><p>A more modern Bowser (Firefox, Chrome, Chromium, Safari...) should be used. Some functions may not work.</p>`
	}
	, views = {
		view: { draggable: 1 }
		, source: {}
		, summary: { title: "Priorities", notEdit: 1 }
	}
	, columns = {
		cols: { draggable: 1, sourceable: 1, step1: [ "size", 0 ]}
		, card: { draggable: 1, size: 'p20' }
		, rows: { draggable: 1, size: 'p50', sourceable: 1, initTable: 1 }
		, fullrow: { draggable: 1, size: 'p100', sourceable: 1, initTable: 1 }
		, top: { class: 'top', size: 'p100', content: "startline" }
		, bottom: { class: 'bottom', size: 'p100', content: "endline" }
		, info: { class: 'top', step1: [ "onoff", 0 ], content: "details"  }
		, overview:  { class: 'top', content: "overview"  }
		, calc: { class: 'bottom' }
	}
	, viewTypes = Object.keys( views )
	, colTypes = Object.keys( columns )
	, types = {		// Attention: not allowed is view and column
		fieldset1: { title: "Main group", step1: [ "size", 0 ], draggable: 1 }
		, fieldset2: { title: "Sub-unit", step1: [ "size", 0 ], draggable: 1 }
		, fieldset3: { title: "Single-unit", step1: [ "size", 0 ], draggable: 1 }
		, explain: { title: "Explanation", step1: [ "size", 0 ], draggable: 1 }
		, example: { title: "Example", step1: [ "size", 0 ], draggable: 1 }
		, image: { title: "Graphic", step1: [ "size", 0 ], draggable: 1 }
		, text: { title: "Fixtext", step1: [ "size", 0 ], draggable: 1 }
		, empty: { title: "Blank area", step1: [ "color", 0 ], step2: [ "size", 0 ]
			, draggable: 1 }
		, choise: { title: "Select list", step1: [ "size", 0 ], draggable: 1 }
		, input: { title: "Input field(s)", step1: [ "size", 0 ], draggable: 1 }
		, supercontrol: { title: "Standard-Control", step1: [ "size", 0 ], draggable: 1 }
		, checkbox: { title: "Check box", step1: [ "size", 4 ], draggable: 1 }
		, radio: { title: "Radio button", step1: [ "size", 4 ], draggable: 1 }
		, extend: { title: "Further", step1: [ "size", 4 ], draggable: 1 }
		, button: { title: "Button", step1: [ "size", 4 ], draggable: 1 }
		, tab: { title: "Tab", step1: [ "size", 4 ], draggable: 1 }
		, card: { title: "Template", step1: [ "color", 1 ], draggable: 1 }
		, task: { title: "Task", step1: [ "task", 1 ]}
		, notePlan: { title: "inform/plan", content: "note" }
		, noteDo: { title: "navigate/contact", content: "note" }
		, noteAct: { title: "change/execute", content: "note" }
		, noteCheck: { title: "check/control", content: "note" }
		, ripePlan: { title: "ripe-P", step1: [ "ripe", 0 ], notEdit: 1 }
		, ripeDo: { title: "ripe-D", step1: [ "ripe", 0 ], notEdit: 1 }
		, ripeAct: { title: "ripe-A", step1: [ "ripe", 0 ], notEdit: 1 }
		, ripeCheck: { title: "ripe-C", step1: [ "ripe", 0 ], notEdit: 1 }
		, cycle: { title: "cycle steps included", step1: [ "cycle", 0 ], notEdit: 1 }
		, autoLevel: { title: "automation level", step1: [ "autoLevel", 0 ], notEdit: 1 }
		, boxtext: { title: "free text", content: " " }
		, boxcheck: { title: "choise", step1: [ "onoff", 1 ], notEdit: 1 }
		, boxradio: { title: "choise", step1: [ "onoff", 1 ], notEdit: 1 }
		, boxlevel: { title: "Estimation", step1: [ "boxlevel", 0 ], notEdit: 1
		, size: 'p30' }
		, boxrisk: { title: "risk level", step1: [ "risk", 0 ], notEdit: 1 }
		, unit: { title: "department/responsibility", content: "&nbsp" }
		, noteInput: { title: "input data or documents", content: "&nbsp" }
		, unitInput: { title: "executing", content: "&nbsp" }
		, noteOutput: { title: "output data or documents", content: "&nbsp" }
		, unitOutput: { title: "Receiving", content: "&nbsp" }
		, remark: { title: "applicable documents, infos etc.", content: "&nbsp" }
	}
	, subTypes = {
		input: { title: 'Format choice', list: { 
			"Web standards": { text: "text row", area: "text area", file: "file"
			, url: "url" }
			, "contact data": { person: "person", towns: "towns", country: "country"
				, email: "email", fon: "fon", }
			, "date and time": { date: "date", week: "week", time: "time"
				, datetime: "date with time", period: "period", timespan: "timespan" }
			, "numeric inputs": { number: "number", float: "float", iban: "iban"
				, vat: "vat", currency: "currency", isounits: "iso-units" }
		}}
		, choise: { title: 'listtyp', list: {
			select: "selection list", multi: "check box list", single: "radio list"
			, toggle: "toggle"
		}}
		, supercontrol: { title: 'control selection', list: {
			 address: "address", login: "login"
			 , appointment: "appointment", bankaccount: "bankaccount"
			 , dynlist: "dynamic list", calendar: "calendar"
			 , timepicker: "timepicker"
		}}
		, extend: { title: 'target setting' }
		, button: { title: 'target setting' }
		, tab: { title: 'target setting' }
	}
	, steps = {
		size: { title: 'width adjustment'
			, class: "p100,p90,p75,p60,p50,p40,p30,p25,p20,p10".split( ',' )
			, descript: "100%,90%,75%,60%,50%,40%,30%,25%,20%,10%".split( ',' )}
		, color: { title: 'color marking'
			, class: "none,neutral,first,second,third,synonym".split( ',' )
			, descript: "empty,dark gray,red,yellow,green,synonym: light gray".split( ',' )}
		, height: { title: 'height adjustment'
			, class: "p100,p75,p50,p30,p25,p20,p10".split( ',' )
			, descript: "100%,75%,50%,30%,25%,20%,10%".split( ',' )}
		, task: { title: 'process stage' 
			, class: "process,keyTask,mainTask,subTask,action".split( ',' )
			, descript: [ "sub process (hours up to day(s)"
				, "elementary process (minutes up to hours)"
				, "main task (½ Minute up to sinutes)"
				, "sub task (8 sec. up to ½ minute)"
				, "action (1 up to 8 sec.)"
			]}
		, ripe: { title: 'ripe level' 
			, class: "ripe0,ripe1,ripe2,ripe3,ripeA".split( ',' )
			, content: "0,1,2,3,A".split( ',' )
			, descript: [ "0 = not fulfilled", "1 = bad fulfilled"
			, "2 = partial fulfilled", "3 = fulfilled", "A = automated" ], inTitle: 1 }
		, risk: { title: 'risk level' 
			, class: "risk0,risk1,risk2,risk3".split( ',' )
			, content: "0,1,2,3".split( ',' )
			, descript: [ "0 = not relevant", "1 = less relevant"
			, "2 = quite relevant", "3 = high relevant" ], inTitle: 1 }
		, cycle: { title: 'Breakdown' 
			, class: "PDAC,PD__,__AC,P___,_D__,___C,__A_".split( ',' )
			, content: "PD<br>CA,PD<br>\u00A0\u00A0,\u00A0\u00A0<br>CA,P\u00A0<br>\u00A0\u00A0,\u00A0D<br>\u00A0\u00A0,\u00A0\u00A0<br>\u00A0A,\u00A0\u00A0<br>C\u00A0".split( ',' )
			, descript: [ "handle / ensure (PDCA)"
			, "create / present (PD)", "manage / release (CA)"
			, "plan / coordinate (P)", "edit / order / deduce (D)"
			, "revise / decide (A)", "check / track (C)" ], inTitle: 1 } 
		, autoLevel: { title: 'automation level' 
			, class: "levelM,levelI,levelW,levelA".split( ',' )
			, content: "M,I,W,A".split( ',' )
			, descript: [ "M = manuell / without system support"
			, "I = interactiv / steering", "W = monitoring / controlling"
			, "A = automatic / algorithmic" ], inTitle: 1 } 
		, onoff: { class: [ "on", "off" ], descript: [ "checked", "inactiv" ]}
		, boxlevel: { class: [ "on", "off" ], descript: [ "selected", "deselected" ]}
		, unitSelect: { class: [ "unit" ], descript: [ "assigned" ]}
	}
	, getExcelXMLformat = function( a ) {
	// excelXMLspreadSheets know format via styles with a reverence by id and formating
	// via direct tags. ColumnWidth is given via an integer field: a, if the field size
	// is higher 50 it is set to line wrapping via style id. 
		return { style: '<Style ss:ID="Default" ss:Name="Normal">'
		+ '<Alignment ss:Vertical="Top"/></Style>'
		+ '<Style ss:ID="firstLine"><Font ss:Bold="1"/></Style>'
  		+ '<Style ss:ID="wrapIt"><Alignment ss:WrapText="1"/></Style>'
	  	, column: (function( a ) {
	   	for( var i = 0, c = ''; i < a.length; i++ )
	   		c += '<Column ss:Width="' + a[ i ] +'"'
	   		+ ( a[ i ] > 50 ? ' ss:StyleID="wrapIt"' : '' )+ '/>';
	   	return c;
	   	})( a )
		};
	}
	, toolTypes = {
		"useCardsort": { col: 'card', sourceEdit: 1, sourceStore: 1
			, types: [ "card" ]
			, toolAct: "card sorting"
			, viewName: "Sorting"
			, views: []
			, appearance: "View change: synonyms on/off and table only"
			, export: "export work data as wiki text"
			, wiki: { type: types.card, column: 'next column' }
		}
		, "usePrototype": { col: 'cols', sourceEdit: 1, sourceStore: 1
			, types: Object.keys( types ).slice( 0, 16 )
			, toolAct: "Prototyping"
			, viewName: "Draft"
			, views: []
			, appearance: "Change of impression: monitor / touch"
			, export: "export work data as wiki text"
		}
		, "useProcessCheck": { col: 'rows', sourceColumns: 'rows', storeEdit: 1, sourceStore: 1
			, types: ( "task,cycle,notePlan,ripePlan,noteDo,ripeDo,noteAct,ripeAct,noteCheck,ripeCheck"
			).split( ',' )
			, toolAct: "Process analysis"
			, appearance: "View change: table only (print view)"
			, viewName: "Process"
			, views: []
			, export: "export work data as excel sheet"
			, excelXML: getExcelXMLformat([ 30,30,30,120,35,130,15,130,15,130,15,130,15 ])
		}
		, "useContext": { col: 'rows', sourceColumns: 'rows', storeEdit: 1, sourceStore: 1
			, types: [ "task", "cycle", "autoLevel" ]
			, toolAct: "Context-of-use analysis"
			, appearance: "View change: table only (print view)"
			, viewName: "Context"
			, views: [ 'summary' ]
			, export: "export work data as excel sheet"
			, excelXML: {}
		}
		, "useProcessMap": { col: 'fullrow', sourceColumns: 'fullrow', storeEdit: 1, sourceStore: 1
			, types: ( "task,cycle,unit,noteInput,unitInput,noteOutput,unitOutput,remark"
			).split( ',' )
			, toolAct: "Process overview"
			, appearance: "View change: table only (print view)"
			, viewName: "Business Unit"
			, views: [ 'summary' ]
			, export: "export work data as excel sheet"
			, excelXML: {}
		}
	}
	, viewData = 'view-view'
	, actType = ( "attr,typeEdit,typeMove,typeCopy,typeDelete,columnSize,columnMove"
	+ ",columnCopy,columnDelete,columnHeader,viewEdit,viewCopy,viewMove,viewDelete"
	+ ",viewActivate,viewJoin,viewReload" ).split( ',' ).concat( Object.keys( steps ))
	, actTypeDesc = "change to,rename to,paste to,copy to,delete"
	+ ",column width up,insert column after,copy column to,delete column"
	+ ",Column header: ,-%&% rename to,-%&% copy to,-%&% paste to"
	+ ",-%&% delete,-%&% activate,new group member,reload"
	, calls = {
		logo: "openHome()"
		, recommend: "sendRecommendation()"
		, initAccount: "initAccount()"
		, addGroup: "addGroup()"
		, numberList: "getNumberCrypt()"
		, showHelp: "showHelp()"
		, addColumn: "addViewCol(this)"
		, addView: "addViewCol(this)"
		, zoomin: "zoom(this)"
		, zoomout: "zoom(this)"
		, appearance: "setAppearance(false)"
		, storeJSON: "storeJson()"
		, storeHTML: "doLocalStore()"
		, export: "storeExport()"
		, privatework: "openPrivateWork(event)"
	}
	, title = {
		logo: homepage +": everything for a professional usability engineering"
		, recommend: "Support and/or recommend this tool to others"
		, initAccount: "useEngineer account: activate moderation access"
		, addGroup: "Create online group\n(password required!)"
		, addView: ' attach'
		, numberList: "Display group or test password as a list of numbers"
		, showHelp: "Show tutorial"
		, addColumn: "Add column"
		, zoomin: "zoom in"
		, zoomout: "zoom out"
		, groupMode: "group mode inactiv"
		, groupOn: 'group mode activ'
		, storeJSON: "Save current state locally\n(drag JSON file here to head to load)"
		, storeHTML: "Save work data locally in this HTML file"
		, export: "Export work data for further processing"
		, privatework: "Open private work window"
	}
	, innerHTML = {
		h1: "List of all group actions"
		, copyLabel: "Copy mode"
		, lockLabel: "Fixed mode"
		, testLabel: "Testing mode"
	}
	, ariaLabel = {
		head: "Steering"
		, trash: "Trash"
		, source: "Stock"
		, table: "Workspace"
	}
	, button = [ 'cancel', 'confirm', 'accept', 'close' ]
  	, data = {
		/*
			fileName: != SHA-512-Hash[ nameLength ]
			, email: "name@email-adresse.xx"
			, cryptBase: randomInt16[ cryptLength ]
			, cryptStart: lengthOfFirstCryptedPattern
			, grouppassword: SHA-512-Hash (salt: groupSalt)
			, title: "head of page"
			, groupName: "mainname of stored file"
			, appearance: "empty or 'touch'"
			, acts: {
				viewIds: [{		// meta id for selectable viewList: 'view-view'
					length of this action string
					, viewEdit,viewCopy,viewMove,viewDelete,viewActivate...
					, viewId
					, viewId or text
					}
					, ...
				]
				, ...
			}
		*/
		fileName: false
		, email: false
		, cryptBase: false
		, cryptStart: false
		, grouppassword: false
		, title: titleDefault
		, groupName: 'Prototype'
		, toolType: 'usePrototype'
		, appearance: ''
		, acts: { 'view-view': [], 'view-source': []}
	}
	, idList = []
	, nameLength = 88
	, cryptLength = 32
	, globalStyle = ""
	, ownHTML = false
	, activeTool = false
	, activeView = false
	, activeId = false
	, networking = false
	, reloadData = false
	, isLocked = false
	, activeCryptLength = 0
	, lastSave = 0
	, lastChange = 0
	, msgReplace = function( m, v ) {
		return msg[ m ].slice( 0 ).replace( /%%/g, v );
	}
	, createDialog = function( type, iH, v, fN, from ) {
	var
		e = $( 'dialogDiv' )
		, input = type == 'prompt' ? '<form>' : ''
		;
		if ( !!e )
			DOM.remove( e );
		e = D.createElement( 'div' );
		if ( input.length ) {
			if ( !Array.isArray( v ))
				v = [{ v: v, l: 200 }];
			for ( var i = 0; i < v.length; i++ )
				input += _getInput( v[ i ]);
			input += '</form>';
		}
		if ( !/<h\d>/.test( iH ))
			iH = '<p>'+ iH +'</p>';
		e.id = 'dialogDiv';
		e.className = type;
		e.innerHTML = '<div><div>'+ iH + input +'</div><div class="buttonRow">'
		+ ( type == 'confirm' || type == 'prompt'	? '<button id="b1">'+ button[ 0 ]
			+ '</button><button id="b2">'
			+ ( type == 'confirm' ? button[ 1 ]	: button[ 2 ])	+'</button>'
			: '<button id="b1">'+ button[ 3 ] +'</button>'
		) +'</div></div>';
		D.body.appendChild( e );
		_setButton( 'b1' );
		_setButton( 'b2' );
		if ( !DOM.isTouch )
			DOM.draggable( e );
		if ( type == 'prompt' ) {
			$( "prompt0" ).select();
			$( 'prompt0' ).focus();
		}
		return e;

		function _setButton( id ) {
			if ( id = $( id ))
				id.addEventListener( 'click', function _clickButton( ev ) {
				var
					e = ev.currentTarget.parentNode.parentNode.parentNode
					;
					ev.preventDefault();
					DOM.remove( e );
					if ( type == 'prompt' && ev.currentTarget.id == 'b2' ) {
						for ( var i = 0, l = e.querySelectorAll( 'textarea,input,select' )
						, r = []; i < l.length; i++ )
							r.push( !l[ i ].type || l[ i ].type != 'checkbox' || l[ i ].checked
							? l[ i ].value.trim() : '' );
						fN( r, from );
					}
					else if ( type == 'confirm' ) 
						fN( ev.currentTarget.id == 'b2' );
				});
		}
		function _getInput( _v ) {
		var
			pw = _v.type && _v.type == 'password'
			? '" onkeyup="useEngineer.checkPassword(this)" autocomplete="off"' : false
			;
			return '<div'+ ( _v.style ? ' style="'+ _v.style +'">' : '>' )
			+ ( !!_v.t ? '<label for="prompt'+ i + ( !_v.type ? '' : '" class="'
			+ _v.type )	+'">'+ _v.t +'</label>' : '' )
			+ ( _v.type && _v.type == 'select'
			? '<div><select id="prompt'+ i +'" '+ ( _v.attr || '' ) +'"><option>'
			+ _v.v.replace( /,/g, '</option><option>' ) +'</option></select></div>'
			: pw || !_v.l || _v.l <= 50 
			? '<div><input id="prompt'+ i +'" type="'+ ( _v.type || 'text' ) +'" value="'
			+ ( _v.v || '' ) + ( pw || '"' )	+ ( _v.attr || '' ) +'></div>'
			: '<div><textarea id="prompt'+ i  +'" maxlength="'+ ( _v.l || 200 ) +'" rows="'
			+ Math.min( 12, Math.ceil( _v.l / 150 )) + ( _v.attr || '' ) +'">'
			+ ( _v.v || '' ) +'</textarea></div>'
			) +'</div>';
		}
	}
	, checkDialog = function( ev ) {
	//@ finish dialog on RETURN or ESC
		if ( !!$( 'dialogDiv' ) && ( ev.keyCode === 13 || ev.keyCode === 27 )) {
		var
			l = $( 'dialogDiv' ).querySelectorAll( 'textarea,input,select' )
			;
			if ( D.activeElement && ev.keyCode === 13 && l.length > 1 ) {
				for( var i = 0; i < l.length -1; i++ ) {
					if ( l[ i ].id == D.activeElement.id ) {
						ev.preventDefault();
						l[ i +1 ].focus();
						return;
					}
				}
			}
			else if ( ev.keyCode === 27 || !D.activeElement
			|| D.activeElement.tagName != 'TEXTAREA' ) {
				ev.preventDefault();
				$(  ev.keyCode === 13 && $( "b2" ) ? "b2" : "b1" ).click();
			}
		}
	}
	, createEmail = function( dialog, subject, body, link, crypt ) {
	var
		e = createDialog( 'email', dialog )
		, a = D.createElement( 'a' )
		;
		body = msg[ body ].slice( 0 ).replace( /%%0/g, link ).replace( /%%1/g, subject )
		+ ( crypt || '' );
		a.href = 'mailto:?subject='+ encodeURI( subject )
		+ '&body='+ encodeURI( body.replace( /<\/br>/g, '\\n\\n'
		).replace( /<\/?[pa]>?/g, '' ).replace( / href="([^"]+)">/g, '$1' ))
		+ '&html-body='+ encodeURI( body );
		a.innerHTML = subject;
		DOM.insertAfter( e.firstElementChild.firstElementChild.lastElementChild, a );
	}
	, setDragDrop = function ( e ) {
	// nessesary for: views, source, trash and column
		$A( e, 'ondragenter', "return useEngineer.allow(event)" );
		$A( e, 'ondragleave', "return useEngineer.allow(event)" );
		$A( e, 'ondragover', "return useEngineer.allow(event)" );
		$A( e, 'ondrop', "useEngineer.drop(event)" );
	}
	, createElement = function( type, id, subType ) {
	var
		e = D.createElement( 'div' )
		, aT = ( type == 'view' ? views : type == 'column' ? columns : types )[ subType ]
		;
		$A( e, 'id', id );
		$CN.add( e, type );
		$CN.add( e, subType  );
		if ( aT.size )
			$CN.add( e, aT.size );
		if ( aT.class )
			$CN.add( e, aT.class );
		if ( type == 'view' ) { //$( 'views' ).children.length !!!!
		var
			n = subType != 'view' && views[ subType ] ? views[ subType ].title || false
			: activeTool.viewName +" "+ data.acts[ viewData ].filter(
			a => a.act == 'viewCopy' ).length
			;
			if ( n )
				$A( e, 'data', n );
			if ( subType == 'view' )
				_setDrag( e );
			$A( e, 'onclick', "useEngineer.click(event)" );
			$A( e.appendChild( D.createElement( 'img' )), 'src', '' );
		}
		else if ( type == 'column' ) {
			if ( aT.content ) {
				$A( e.appendChild( D.createElement( 'div' )), 'class', 'columnHeader' );
				e.lastElementChild.innerHTML = aT.content;
			}
			if ( !!aT.step1 || !!aT.draggable )
				e.appendChild( D.createElement( 'div' ));
			if ( !!aT.step1 )
				_setEvent( e.lastElementChild, aT.step1 );
			if ( !!aT.draggable ) {
				setDragDrop( e );
				_setDrag( e.lastElementChild );
				if ( !aT.step1 )
					$CN.add( e.lastElementChild, 'size' );
			}
		}
		else {
			if ( aT.title )
				$A( e, 'title', aT.title );
			if ( !!aT.draggable )
				_setDrag( e );
			if ( !!aT.step1 )
				_setEvent( e.appendChild( D.createElement( 'div' )), aT.step1 );
			if ( !!aT.step2 )
				_setEvent( e.appendChild( D.createElement( 'div' )), aT.step2 );
			_setEdit( e.appendChild( D.createElement( 'div' )));
			if ( subType in subTypes )
				_setSubType( D.createElement( 'div' ), subType );
		}
		return e;

		function _setEvent( _e, t ) {
			$CN.add( _e, t[ 0 ]);
			$CN.add( _e.parentNode, steps[ t[ 0 ]].class[ t[ 1 ]]);
			if ( steps[ t[ 0 ]].content )
				_e.innerHTML = steps[ t[ 0 ]].content[ t[ 1 ]];
			$A( _e, 'onclick', "useEngineer.step(event,'"+ t[ 0 ] +"')" );
			$A( _e.parentNode, 'data', steps[ t[ 0 ]].descript[ t[ 1 ]]);
		}
		function _setDrag( _e ) { 
			$A( _e, 'draggable', "true" );
			$A( _e, 'ondragstart', "useEngineer.dragstart(event)" );
			$A( _e, 'ondrag', "return useEngineer.allow(event)" );
			if ( DOM.isTouch )
				$A( _e, 'ontouchmove', 'useEngineer.DOM.draggTouch(event)' );
		}
		function _setEdit( _e ) {
			_e.innerHTML = aT.content ? ( aT.content.length ? aT.content : ' ' )
			: !aT.notEdit ? aT.title : '';
			$CN.add( _e, 'content' );
			if ( !aT.notEdit ) {
				$A( _e, 'onfocus', "useEngineer.typeEdit(event,'focus');" );
				$A( _e, 'oninput', "useEngineer.typeEdit(event,'input');" );
				$A( _e, 'onblur', "useEngineer.typeEdit(event,'blur');" );
				$A( _e, 'onclick', "useEngineer.setEditable(event,'"
				+ ( activeTool.sourceEdit ? 'all' : activeTool.storeEdit ? 'some' : 'none' )
				+ "');" );
			}
			else
				$A( _e, 'onclick', "useEngineer.step(event,'"+ aT.step1[ 0 ] +"')" );
		}
		function _setSubType( _e, t ) {
			if ( t == 'input' || t == 'supercontrol' ) {
				_e = _e.appendChild( D.createElement( 'select' ));
				_e.innerHTML = __getOptions( subTypes[ t ].list, t == 'input' );
				_e.selectedIndex = 0;
				$A( _e, 'onchange', "useEngineer.select(event);" );
				$A( _e.parentNode, 'data', _e.options[ 0 ].value );
				e.appendChild( _e.parentNode );
			}
			else {
				_e = e.appendChild( _e );
				if ( t == 'choise' )
					$CN.add( _e, 'select' );
				$A( _e, 'onclick', "useEngineer.edit(event,'"+ t +"');" );
			}
			$A( _e, 'title', subTypes[ t ].title[ t ]);
			$CN.add( e.lastElementChild, 'attr' );

			function __getOptions( list, OPTGROUP ) {
			var
				s = ''
				;
				if ( OPTGROUP )
					for ( var g in list )
	               s += '<optgroup label="' + g + '">'+ __get( list[ g ])
	            	+ '</optgroup>\n';
	         else
	            s = __get( list );
	         return s;

	         function __get( l ) {
	         var
	         	_s = ''
	         	;
	            for ( var o in l )
	               _s += '<option value="' + o + '">' + l[ o ] + '</option>';
	            return _s ;
	         }
			}
		}
	}
	, addTypes = function() {
	var
		aT = activeTool.sourceColumns
		, src = $( 'source' )
		;
		if ( !!aT ) {
			src = createElement( 'column', 'column-'+ aT, aT );
			$( 'source' ).appendChild( src );
		}
		aT = activeTool.types
		for ( var i = 0, t, e, s; aT && i < aT.length; i++ ) {
			t = aT[ i ];
			e = createElement( 'type', 'type-'+ t, t );
			src.appendChild( e );
		}
	}
	, getAjax = function() {
		clearTimeout( getAjax.timeout );
		getAjax.timeout = setTimeout( function() {
			if ( !reloadData && !!networking )
				ajax( 'GET', path +'exchange/exchange.php?fn='+ data.fileName, null
				, loadPartFile, 4500, true );
			getAjax(); // next period
		}, 2000 );			
	}
	, loadPartFile = function( t, PARTIAL=false ) {
		if( !!t ) {
			if ( t.length && t != -1 ) {
			var
				acts = loadActs( '\0'+ cryptOut( PARTIAL ? t
				: t.slice( activeCryptLength )))
				, act = acts ? acts[ acts.length -1 ] : false
				;
				// here is check if the view id exists else active joinId has been 
				// changed, that means all data have been replaced by addGroup
				if ( !act || !act.act || !data.acts[ act.view ]) {
					reloadData = true;
					if ( activeCryptLength !== 0 ) {
						act = createDialog( 'error', msg.reloadData );
						setTimeout( function() { DOM.remove( act ); }, 4500 );
					}
					activeCryptLength = 0;
					loadFullFile();
				}
				else {
					setActs( acts, true );
					if ( PARTIAL )
						activeCryptLength += t.length;
					else
						activeCryptLength = t.length;
				}
			}
			if ( getAjax.wait ) {
				getAjax.wait();
				getAjax.wait = false;
			}
		}
		else {
			setNetworking( false );
			createDialog( 'error', msg.newGroupMissing );
		}

	}
	, loadFullFile = function( NOTIMEMSG=false ) {
	var
		time = /^(1|2)\d{9,9}_/.test( data.fileName )// check creating time...
		? $I( data.fileName.slice( 0, 10 )) : false	// ...till in 20 years
		;
		ajax( 'GET', path +'exchange/exchange.php?fn='+ data.fileName
		, null, function( t ) {
			if ( t !== false ) {
				activeCryptLength = t.length;
				t = cryptOut( data.cryptStart + t );
				if ( t.indexOf( cryptValidation ) > -1 ) {
					idList = [];
					joinCheck.joins = [];
					if ( t = loadActs( t, true )) {
					var
						viewAfter = ""
						;
						data.acts = _getData( t );
						setAllViews( viewAfter );
						joinCheck.checkId( joinCheck.myJoin );
						setNetworking( true );
						lastSave = lastChange = 0;
						getAjax(); // reset period
						if ( time && !NOTIMEMSG )
							_setTestingInfo();
					}
				}
				else
					createDialog( 'error', msg.errorLink );
			}
			else
				createDialog( 'error', msg.noGroup );
			reloadData = false;

			function _getData( a ) {
				for ( var i = 0, view = '', acts = {}; i < a.length; i++ ) {
					view = a[ i ].view;
					if ( !acts[ view ])
						acts[ view ] = [];
					if ( a[ i ].act == 'viewActivate' )
						viewAfter = a[ i ].id;
					else if ( a[ i ].act == 'viewJoin' )
						joinCheck.setId( a[ i ].data );
					else if ( a[ i ].id != '0-0' )
						acts[ view ].push( a[ i ]);
				}
				return acts;
			}
			function _setTestingInfo() {
				time = 20 - $I(( Date.now() / 1000 - time ) / 60 );
				if ( time >= 2 ) {
					createDialog( 'help', msgReplace( 'testingTime', time ));
					setTimeout( function() {
						createDialog( 'help', msgReplace( 'testingTime', 2 ));
					}, ( time -2 ) * 60000 );
				}
			}
		}, 4500 );
	}
	, getAct = function( a, v, d, aId ) {
		return {
			act: a
			, view: v
			, id: aId ? aId : activeId || getRandId()
			, data: d
		};
	}
	, cleanEdit = function( e ) {
		e = e && e.innerHTML ? e.innerHTML.replace( /(<div>)?<\/?br>(<\/div>)?/, '\n'
		).replace( /<div>([^<]*)?<\/div>/g, '\n$1' ).replace( /<\/?[^>]*?>/g, ''
		).replace( /&nbsp;/g, ' ' ).trim() : '';
		return e.length ? e : '\u00A0';
	}
	, getRandId = function() {
		return joinCheck.myId +'-'+ getRandom16( 1 );
	}
	, getId = function( _idList=false ) {
	var
		tries = 1000
		, id
		;
		_idList = _idList || idList;
		do {
			id = getRandId();
		} while( _idList.indexOf( id ) > -1 && --tries );
		if ( tries > 0 ) {
			_idList.push( id );
			return id;
		}
		else
			return false; 
	}
	, getType = function( e ) {
		return e.id == 'views' ? 'views'
		: e.id == 'source' ? 'source'
		: e.id == 'table' ? 'table'
		: e.id == 'trash' ? 'trash'
		: $CN.contains( e, 'view' ) ? 'view'
		: $CN.contains( e, 'type' )	? 'type'
		: $CN.contains( e, 'column' ) ? 'column'
		: false;
	}
	, getClassAct = function( view, e, tId, type ) {
		for ( var i = 0, c = steps[ type ].class; i < c.length; i++ )
			if ( $CN.contains( e, c[ i ]))
				break;
		return getAct( $CN.contains( e, 'column' ) ? 'columnSize' : type, view, c[ i ]
		, tId );
	}
	, getActList = function( acts, view, sId, tId, newId ) {
	var
		type = $CN.contains( $( sId ), 'type' ) ? 'type' : 'column'
		// get copy of from all acts with source id [sId] (don't touch the originals!)
		, newActs = JSON.parse( JSON.stringify( acts.filter(
			a => a.id == sId && !/move/i.test( a.act )	// moves not needed
		)))
		, found = {}
		;
		// reduce newActs from behind to last change and set new id and view 
		newActs = newActs.reverse().filter( function( a ) {
			if ( !found[ a.act ]) {
				found[ a.act ] = 1;
				a.id = newId;
				a.view = view;
				if ( a.act.slice( -4 ) == 'Copy' )
					type = a.act.slice( 0, -4 );
				return true; 
			}
			else
				return false;
		}).reverse();
		// put copy act in front if the source is a template
		if ( /^[vct]/.test( sId ))
			newActs.unshift( getAct( type +'Copy', view, sId, newId ));
		// put the nessesary moveAct to target [tId] in second position
		if ( tId != 'none' )
			newActs.splice( 1, 0, getAct( type +'Move', view, tId, newId ));
		return newActs;
	}
	, getColumnActs = function( acts, view, sId, tId, newId ) {
	//@ get first maximum type id, take number and count it up to create new type id
	//@ REMARK: start with 1 because first is sizer & dragger
	var
		newActs = getActList( acts, view, sId, tId, newId )
		, l = $( sId )
		;
		if ( l && ( l = l.children )) {
			for ( var i = 0, lastId = newId, _id; i < l.length; i++ ) {
				if ( !$CN.contains( l[ i ], 'size' )) {
					_id = getId();
					newActs = newActs.concat( getActList( acts, view, l[ i ].id, lastId
					, _id ));
					lastId = _id;
				}
			}
		}
		return newActs;
	}
	, getSourceActs = function( acts, id, ids ) {
	var
		last = $( 'source' ).querySelectorAll( '[id^="type-"]' )
		, del = acts.filter( a => a.act == 'typeDelete' ).map( a => a.id )
		, a = acts.filter( a => a.act == 'typeCopy' && del.indexOf( a.id ) < 0 )
		, newActs =[]
		, newId
		;
		last = last[ last.length -1 ].id;
		for ( var i = 0; i < a.length; i++ ) {
			newId = getId( ids );
			newActs = newActs.concat( getActList( acts, id, a[ i ].id, last, newId ));
			last = newId;
		}
		return newActs;
	}
	, getViewActs = function( acts, view, newIds=false ) {
	//@ optimize all acts in a view: get finished order off all elements without between
	//@ orders and deleted elements... and change the view in each act!!
	//@ IMPORTANT: it's extremely important to create a copy of acts, because acts in 
	//@ the struct are changed (especially the view) and as javascript given pointer 
	//@ this would impact the original... well, acts.slice(0) is not working! It is also
	//@ nessesary to examine the hole list because the view might not be on screen and
	//@ therefore no DOM-elements available to get a order list...
	//@ REMARK: in some cases users may edit the JSON file, it is also tried to ensure
	//@ that. Therefore a order must be calulated and specified. It is appended as order:
	//@ key to the returned newActs-Array. This prevents to calc it twice on export! 
	var
		list = acts.filter( a =>
		/typeCopy|typeMove|typeDelete|columnCopy|columnMove|columnDelete/.test( a.act )
		&& !/^[vct]/.test( a.id ))
		, cols = [ 'column' ]	// nessesary if column is moved to first position
		, newActs = []
		, order = []
		, oldOrder = {}
		;
		for ( var i = 0, a, p; i < list.length; i++ ) {
			a = list[ i ];
			if ( a.act == 'columnDelete' || a.act == 'typeDelete' ) {
				if ( a.act == 'columnDelete' ) {
					_delete( cols, a.id );
					_delete( order, 'c-'+ a.id );
				}
				else
					_delete( order, a.id );
			}
			else if ( a.act == 'columnCopy' ) {
				_add( cols, a.id );
				_add( order, 'c-'+ a.id );
			}
			else if ( a.act == 'columnMove' ) {
				_delete( cols, a.id );
				_insert( cols, a.data.replace( /column-.+$/, 'column' ), a.id );
			}
			else {
				_delete( order, a.id );
				_insert( order, ( cols.indexOf( a.data ) < 0 ? '' : 'c-' ) + a.data, a.id );
			}
		}
		cols.shift();
		// create all columns in correct sequence...
		order = order.join( ',' ).split( /,?c-/ ).filter( a => a.length > 0 );
		order = order.sort(( a, b ) =>
			cols.indexOf( a.slice( 0, ( a +',' ).indexOf( ',' )))
			- cols.indexOf( b.slice( 0, ( b +',' ).indexOf( ',' )))
		);
		// create all types
		list = order.length ? order.join( ',' ).split( ',' ) : [];
		for ( var i = 0, p = -1, id; i < list.length; i++ ) {
			if ( order[ p +1 ] && order[ p +1 ].indexOf( list[ i ]) === 0 )
				order[ ++p ] = [];
			id = getId( newIds );
			oldOrder[ id ] = list[ i ];
			order[ p ].push( id );
			if ( newIds )
				newIds.push( id );
		}
		// move all types to correct position
		for ( var i = 0, last = 'none'; i < order.length; i++ ) {
			list = order[ i ];
			newActs = newActs.concat( getActList( acts, view, oldOrder[ list[ 0 ]], last
			, list[ 0 ]))
			for ( var j = 1; j < list.length; j++ ) {
				newActs = newActs.concat( getActList( acts, view, oldOrder[ list[ j ]]
				, list[ j -1 ], list[ j ]));
			}
			last = list[ 0 ];
		}
		if ( newIds )
			newActs.idList = newIds;
		return newActs;

		function _add( l, id ) {
			if ( l.indexOf( id ) == -1 )
				l.push( id );
		}
		function _delete( l, id ) {
			if (( id = l.indexOf( id )) > -1 )
				l.splice( id, 1 );
		}
		function _insert( l, t, id ) {
			if (( t = l.indexOf( t )) > -1 )
				l.splice( t +1, 0, id );
		}
	}
	, typeCopy = function( tId, sId ) {
		if ( $( tId ))			// in case of view change and type in source...
			DOM.remove( $( tId ));
		$( 'table' ).firstElementChild.appendChild( createElement( 'type', tId
		, sId.slice( 5 )));
	}
	, columnCopy = function( tId, sId ) {
		if ( $( tId ))			// in case of view change and column in source...
			DOM.remove( $( tId ));
		$( "table" ).appendChild( createElement( 'column', tId, sId.slice( 7 )));
	}
	, viewCopy = function( tId, sId ) {
		if ( !$( tId ))
			$( "views" ).appendChild( createElement( 'view', tId, sId.slice( 5 )));
	}
	, setClass = function( e, type, isClass ) {
	var
		a = steps[ type ].class
		;
		for ( var i = 0; i < a.length; i++ )
			if ( $CN.contains( e, a[ i ])) {
				$CN.remove( e, a[ i ]);
				i = !!isClass ? a.indexOf( isClass ) : i +1 >= a.length ? 0 : i +1;
				$CN.add( e, a[ i ]);
				if ( !!isClass ) {
					if ( e = e.querySelector( '.'+ type )) {
						$A( e.parentNode, 'data', steps[ type ].descript[ i ]);
						if ( !!steps[ type ].content )
							e.innerHTML = steps[ type ].content[ i ];
					}
				}
				return a[ i ];
			}
	}
	, getClass = function( e, type ) {
		type = steps[ type ].class;
		for ( var i = 0; i < type.length; i++ )
			if ( $CN.contains( e, type[ i ]))
				return type[ i ];
		return type[ 0 ];
	}
	, setChoise = function( e, s ) {
	//@ depending of count of entries  or leading sign the choise gets a other apearence
	var
		l = s.split( '\n' )
		, type = s[ 0 ] == '+' ? 'multi' : s[ 0 ] == '-' ? 'single'
		: l.length == 2 ? 'toggle' : 'select'
		, mark = type == 'single' ? '\uD83D\uDD18 ' : '\u2611\uFE0F ' // multi
		;
		if ( type == 'multi' || type == 'single' )
			l[ 0 ] = l[ 0 ].slice( 2 );
		e.className = 'attr';
		$CN.add( e, type );
		$A( e, 'data', l.join( '\n' ));
		if ( type != 'select' )
			$A( e, 'data-test', l.length == 2 ? l.pop() : mark + l.join( '\n'+ mark ));
	}
	, setNetworking = function( ON = false ) {
		networking = ON;
		$CN[ ON ? 'add' : 'remove' ]( $( "groupMode" ), 'networking' );
		$A( $( "groupMode" ), 'title', ON ? title.groupOn : title.groupMode );
	}
	, activate = function( e, onTest ) {
		activeEdit = { start: false, acts: []};
		if ( !!onTest && isLocked ) {
			e = $A( e.lastElementChild || e, 'data' );
			if ( !!e && e.length
			&& ( e = $( 'views' ).querySelector( '[data="'+ e +'"]'))) {
				_clean( $( 'table' ));
				activeView = e.id;
				setActs( data.acts[ e.id ]);
				initTest();
			}
		}
		else {
			$CN.remove( $( activeView ), 'active' );
			$CN.add( e, 'active' );
			DOM.scrollIntoView( e );
			if ( e && activeView != e.id ) {
				_clean( $( 'table' ));
				_clean( $( "protocol" ));
				activeView = e.id;
 				if ( e.id.indexOf( 'view-' ) > -1 ) {
 				var
 					acts = checkSend( e.id, 'viewCopy', e.id, true )
 					;
 					acts.shift();
 					setActs( acts );
 				}
				else
					setActs( data.acts[ e.id ]);
				setViewShot( activeView );
				setTimeout( function() { DOM.scrollIntoView( $( 'table' ).firstChild ); }
				, 3000 );
			}
		}

		function _clean( t ) {
			while ( !!t.lastElementChild && t.lastElementChild.tagName != 'H1' )
					DOM.remove( t.lastElementChild );
		}
	}
	, checkSend = function( view, a, v, GETSEND=false ) {
	//@ REMARK: 'Copy' always contains also a 'Move', but the active element has to be
	//@ duplicated. Therefore some additional acts can be nessesary to create an identic
	//@ content of this element. That means, repeat all acts (size, edit ...) for the same
	//@ view and add 'Move'... 
	var
		type = a.slice( 0, 4 )	// cuts colu-mn!!
		// no target => create empty view or column
		, empty = v === -1 ? 'emptyView' : /view-/.test( v ) ? v.slice( 5 ) : false
		, dataView = view
		, isCopy = $( "copytoggle" ).checked
		, acts = []
		, id
		;
		if ( a.slice( -4 ) == "Move" || a.slice( -4 ) == "Copy" ) {
			id = ( id = $( activeId )) && id.parentNode ? id.parentNode.id : false;
			if ( !empty && id && (( view === "view-source" && id != 'source' )
			|| ( view !== "view-source" && id == 'source' ))) {
			// to hand copies from and to source correct on multi views, they have to be
			// deleted on old place and copied to the new one
				dataView = id == 'source' ? "view-source" : activeView;
				a = a.slice( 0, -4 ) +'Copy';
				if ( !isCopy && dataView != view && !/^[ct]/.test( activeId ))
					acts = checkSend( dataView, a.slice( 0, -4 ) +'Delete', activeId, true );
			}
			else if ( isCopy || /^[ct]/.test( activeId ))
				a = a.slice( 0, -4 ) +'Copy';
		}
		if ( a.slice( -4 ) == "Copy" ) {
			id = getId();
			if ( type == 'type' )
				acts = acts.concat( getActList( data.acts[ dataView ] || [], view, activeId
				, v, id ));
			else if ( type == 'view' ) {
				acts.push( getAct( a, viewData, viewData, id ));
				if ( empty )
					_getEmptyView( empty );
				else {
					acts.push( getAct( 'viewMove', viewData, activeId, id ));
					acts = acts.concat( getViewActs( data.acts[ activeId ] || [], id
					, false ));
				}
				acts.push( getAct( 'viewActivate', viewData, getRandId(), id ));
			}
			else {
				if ( empty )
					empty = 'column-'+ activeTool.col;
				acts = acts.concat( getColumnActs( data.acts[ dataView ] || [], view
				, empty || activeId, empty || v, id ));
			}
		}
		else if ( a == 'columnDelete' ) {
		var
			e = $( activeId ).firstElementChild
			;
			while ( !!( e = e.nextElementSibling ))	// delete all elements in column
				acts.push( getAct( 'typeDelete', activeView, getRandId(), e.id ));
			acts.push( getAct( a, view, v ));
		}
		else {
			acts.push( getAct( a, view, v == 'views' ? viewData : v ));
			if ( a == 'viewDelete' && activeView == activeId )
				acts.push( getAct( 'viewActivate', viewData, getRandId(), Object.keys(
					data.acts ).filter( v => v !== activeId && !/^v/.test( v ))[ 0 ]
				));
		}
		if ( GETSEND )
			return acts;
		sendActs( acts );
		activeId = false;

		function _getEmptyView( name ) {
			id = name == 'emptyView' ? id : v;
			for( var i = 0, l = useEngineer[ name ], last = l[ 0 ].data, aId, type, move
			; i < l.length; i++ ) {
				v = l[ i ].data;
				if ( l[ i ].act.slice( -4 ) == 'Copy' ) {
					aId = getId();
					type = l[ i ].act.slice( 0, -4 );
					acts.push( getAct( l[ i ].act, id, type +'-'+ v, aId ));
					if ( type == 'column' ) {
						if ( !columns[ v ] || !columns[ v ].initTable ) {
							acts.push( getAct( 'columnMove', id, ( columns[ last ]
							? 'column-' : '' ) + last, aId ));
							last = aId;
						}
						else {
							move = last = aId;
							for ( var j = 0, aT = activeTool.types; j < aT.length; j++ ) {
								aId = getId();
								acts.push( getAct( 'typeCopy', id, 'type-'+ aT[ j ], aId));
								acts.push( getAct( 'typeMove', id, move, aId ));
								move = aId;
							}
						}
					}
					else if ( move )
						acts.push( getAct( type +'Move', id, move, aId ));
					move = aId;
				}
				else
					acts.push( getAct( l[ i ].act, id, v, aId ));
			}
		}
	}
	, sendActs = function( acts, GETSEND = false ) {
		if ( networking || GETSEND ) {		// only _send if initialized...
		var
			s = ""
			, f
			;
			for ( var i = 0, a, v; i < acts.length; i++ ) {
				a = acts[ i ];
				v = a.data;
				if ( a.act in steps )
					v = _transData( steps[ a.act ].class, v );
				else {
					switch ( a.act ) {
						case 'viewJoin':
						case 'viewEdit':
						case 'typeEdit':
						case 'columnHeader':
						case 'attr': break;
						case 'columnSize':
							v = _transData( steps.size.class, v ); break;
						case 'viewCopy': v = _transData( viewTypes, v.slice( 5 )); break;
						case 'columnCopy': v = _transData( colTypes, v.slice( 7 )); break;
						case 'typeCopy': v = _transData( activeTool.types, v.slice( 5 )); break;
						default:	v = _transId( v ); break;
					}
				}
				s += String.fromCharCode( v.length +5 ) + _transData( actType, a.act )
				// obfuscate standard view
				+ ( a.act.slice( 0, 4 ) == 'view' ? getRandomUnicode( 2 )
				: _transId( a.view ))
				+ _transId( a.id )
				+ v;
			}
			if ( GETSEND )
				return s;
			else if ( !reloadData && s.length ) {
				getAjax(); // reset period and send-/setActs immediately
				ajax( 'POST', path +'exchange/exchange.php?gw='+ data.grouppassword
				, data.fileName + cryptIn( s ), loadPartFile, 4500, true );
			}
		}
		else
			setActs( acts, true );
		
		function _transData( base, _v ) {
			return String.fromCharCode( base.indexOf( _v ));
		}
		function _transId( id ) {
			id = id.split( '-' );
			return id[ 0 ] == 'view' ? '\u0001'+ getRandomUnicode( 1 )
			: String.fromCharCode( $I( id[ 0 ])) + String.fromCharCode( $I( id[ 1 ]));
		}
	}
	, sendDropList = function( s, view, t, tType ) {
	var
		ids = s.match( /\d+-\d+/g ).filter( function( v, p, self ) { // find all ids...
 			return self.indexOf( v ) == p;		// ... and get unique ones
		})
		;
		for( var i = 0; i < ids.length; i++ )	// replace all found ids by new ones
			s = s.replace( new RegExp( ids[ i ], 'g' ), getId());
		if ( tType != 'view' )
			s = s.replace( /("view":")[^"]+/g, "$1"+ view );
		s = JSON.parse( s );
		if ( !!t ) {
			// insert the correct move position on drop via duplicate the first copy act...
			s.acts.unshift( JSON.parse( JSON.stringify( s.acts[ 0 ])) );
			s.acts[ 1 ].act = s.type +"Move";	// ... change act ...
			s.acts[ 1 ].data = t.id;				// ... set position
			sendActs( s.acts );
		}
	}
	, loadActs = function( acts, ALL=false ) {
	var
		pos = acts.charCodeAt( 0 )
		, a
		;
		if ( ALL ) {
			a = acts.substr( 1, pos ).split( ',' );
			data.appearance = a.pop();		// cut last
			data.title = a.pop();			// cut second last
			$( 'title' ).innerHTML = data.title.length ? data.title : titleDefault;
			data.acts = {};
		}
		return _getActions( _getArray( acts.slice( pos +1 )));

		function _getArray( s ) {
		var
			a = []
			;
			while ( s.length ) {
				a.push( s.substr( 1, s.charCodeAt( 0 )));
				s = s.slice( s.charCodeAt( 0 ) +1 );
			}
			return a;
		}
		function _getActions( a ) {
			for ( var i = 0, act, js, c, lastActivate = false, has = false; i < a.length
			; i++ ) {
				if ( act = actType[ a[ i ].charCodeAt( 0 )]) {
					js = {
						act: act
						, view: act.slice( 0, 4 ) == 'view' ? viewData : __setId( 1, 2 )
						, id: __setId( 3, 4 )
					};
					if ( act.slice( -4 ) == 'Copy' && idList.indexOf( js.id ) < 0 )
						idList.push( js.id );
					c = a[ i ].charCodeAt( 5 );
					if ( js.act in steps )
						js.data = steps[ js.act ].class[ c ];
					else {
						switch ( js.act ) {
							case 'viewReload': return false;
							case 'viewJoin':
							case 'viewEdit':
							case 'typeEdit':
							case 'columnHeader':
							case 'attr': js.data = a[ i ].slice( 5 ); break;
							case 'columnSize': js.data = steps.size.class[ c ]; break;
							case 'columnCopy': js.data = 'column-'+ colTypes[ c ]; break;
							case 'typeCopy': js.data = 'type-'+ activeTool.types[ c ]; break;
							case 'viewCopy': has = true; js.data = 'view-'+ viewTypes[ c ];
								break; // no view its reload or damaged
							case 'viewActivate': js.act = 'double'; lastActivate = i;
							default:	js.data = __setId( 5, 6 );	break;
						}
					}
					a[ i ] = js;
				}
			}
			if ( lastActivate !== false )
			// if there some parallel viewActivate actions take the last one
				a[ lastActivate ].act = 'viewActivate';
			return !ALL || has ? a : false;

			function __setId( id1, id2 ) {
				id1 = a[ i ].charCodeAt( id1 );
				return id1 == 1 ? 'view-source' : id1 +'-'+ a[ i ].charCodeAt( id2 );
			}
		}
	}
	, setActs = function( acts, APPEND=false ) {
		for ( var i = 0, a, e, t, eC, pN; i < acts.length; i++ ) {
			pN = ''; 				// protocolName := remember old name for protocol
			a = acts[ i ];
			e = $( a.id );
			t = $( a.data );
			if ( APPEND )
				_pushIntoCorrectView();
			if ( a.act == 'viewActivate' ) {
				if ( APPEND ) {
					if ( e && data.acts[ a.id ])
						activate( e, false );
					else if ( networking ) {
						reloadData = true;
						createDialog( 'error', msg.reloadData );
						loadFullFile();
						return;
					}
				}	
			}
			else if ( a.view == activeView || a.view.indexOf( 'view-' ) > -1 ) {
				if ( a.act in steps ) {
					setClass( e, a.act, a.data );
					_setCalc( a, e );
				}
				else {
					switch ( a.act ) {
						case 'viewJoin': if( APPEND ) joinCheck.setId( a.data ); break;
						case 'viewEdit':
							_checkEdit();
							pN = $A( e, 'data' );
							$A( e, 'data', a.data );
							break;
						case 'typeEdit':
							if ( _checkEdit( true ) && ( eC = e.querySelector( '.content' ))) {
								pN = eC.innerHTML;
								eC.innerHTML = a.data.replace( /\n/g, '<br>' );
								eC = e.className.replace( /type/, '' ).trim();
								if ( useEngineer.calc[ eC ])
									useEngineer.calc[ eC ]( data.acts, a, eC );
							}
							break;
						case 'columnSize': setClass( e, 'size', a.data ); break;
						case 'columnHeader': eC = e.firstElementChild;
							if ( $CN.contains( eC, 'columnHeader' ))
								eC.innerHTML = useEngineer.calc[ a.data ]
								? useEngineer.calc[ a.data ]( data.acts, activeTool, data.title )
								: useEngineer.info[ a.data ] || (  a.data );
							break;
						case 'attr':
							pN = $A( e.lastElementChild, 'data' );
							if ( $CN.contains( e, 'choise' ) && _checkEdit( true ))
								setChoise( e.lastElementChild, a.data );
							else {
								_checkEdit();
								$A( e.lastElementChild, 'data', a.data );
							}
							break;
						case 'viewCopy': viewCopy( a.id, a.data ); break;
						case 'columnCopy': columnCopy( a.id, a.data ); _setCalc( a, $( a.id ));
							break;
						case 'typeCopy': typeCopy( a.id, a.data ); break;
						case 'viewDelete': delete data.acts[ a.id ];
						case 'columnDelete': DOM.remove( e ); _setCalc( a, false ); break;
						case 'typeDelete': _checkEdit(); DOM.remove( e ); break;
						case 'viewMove':
						case 'columnMove':
							if ( !t ) {
								if ( a.view == 'view-source' )
									$( 'source' ).appendChild( e );
								else {
									t = $( a.act == 'viewMove' ? 'views' : 'table' );
									if ( t.firstElementChild )
										DOM.insertBefore( t.firstElementChild, e );
									else
										t.appendChild( e );
								}
							}
							else
								DOM.insertAfter( t, e );
							break;
						case 'typeMove':
							if ( !t && a.view == 'view-source' )
								$( 'source' ).appendChild( e );
							else if ( $CN.contains( t, 'column' )) {
								if ( t.firstElementChild )
									DOM.insertAfter( t.firstElementChild, e );
								else
									t.appendChild( e );
							}
							else
								DOM.insertAfter( t, e );
							break;
					}
				}
				if ( APPEND
				&& !/delete|size|attr|column|Edit|Activate/.test( a.act ))
					delayRemove( e, 'shake', 700 );
			}
			_setProtocol();
		}
		if ( APPEND ) {
			lastChange = new Date();
			setViewShot( activeView );
		}
		function _setCalc( a, e ) {
			if ( useEngineer.calc[ a.act ])
				useEngineer.calc[ a.act ]( data.acts, e, steps[ a.act ].class );
		}
		function _pushIntoCorrectView() {
			if ( !data.acts[ a.view ])
				data.acts[ a.view ] = [];
			data.acts[ a.view ].push( a );
		}
		function _checkEdit( STORE ) {
		//@ in case of a parallel editing the doubleEdit value is stored in activeEdit.
		//@ If the edited text may be longer, users may have a choise to copy the hole
		//@ type. Especially not to interrupt the direct editing of text the node is 
		//@ cloned and inserted after with the overwritten value. The edited node gets
		//@ a new copy id! The copied node may be removed on user confirmation later
			if ( activeEdit.start && activeEdit.id == a.id ) {
				activeEdit.doubleEdit = a.data;
				$CN.add( a.act == 'typeEdit' ? e : D.activeElement.parentNode, 'doubleEdit' );
				return false
			}
			return true;
		}
		function _setProtocol() {
		var
			h2 = D.createElement( 'h2' )
			, v = ''
			;
			if (( !pN || !pN.length ) && e && a.act[ 0 ] != 'c' )
				pN = a.view == viewData ? $A( e, 'data' ) : cleanEdit( e.children[ 1 ]);
			if ( a.act in steps ) {
				v = steps[ a.act ].title +' '+ actTypeDesc[ actTypeDesc.length -1 ]+' '
				+ steps[ a.act ].descript[ steps[ a.act ].class.indexOf( a.data )];
			}
			else {
				switch ( a.act ) {
					case 'viewCopy': case 'columnCopy': case 'typeCopy': v = false; break;
					case 'columnSize':
						v = steps.size.descript[ steps.size.class.indexOf( a.data )]; break;
					case 'attr':
					case 'viewEdit':
					case 'typeEdit': v = a.data; break;
					default:
						if ( !!t )
							v = cleanEdit( t.children[ 1 ]);
						break;
				}
				v = actTypeDesc[ actType.indexOf( a.act )] +' '+ v;
			}
			if ( v !== false ) {
				h2.innerHTML = ( pN +' ' || '' ) + v;
				$( "protocol" ).appendChild( h2 );
			}
		}
	}
	, setAllViews = function( viewAfter, CLEAN=true ) {
		$( 'title' ).innerHTML = data.title && data.title.length	? data.title : titleDefault;
		useEngineer.setAppearance( data.appearance || '' );
		if ( CLEAN ) {
			if ( activeTool.sourceStore )
				_cleanBord( $( 'source' ));
			_cleanBord( $( 'views' ));
			_cleanBord( $( 'table' ));
			_cleanBord( $( 'protocol' ));
		}
		setActs( data.acts[ viewData ]);
		for ( var i = 0, l = Object.keys( data.acts ).reverse(); i < l.length; i++ ) {
 			if ( $( l[ i ]) && l[ i ] != activeView )
				activate( $( l[ i ]));
		}
		activeTool.views.forEach( function( id ) {	// activate intern created views
			if ( 'view-'+ id != viewAfter )
				activate( $( 'view-'+ id ));
		});
		if ( viewAfter && activeView != viewAfter )
			activate( $( viewAfter ));						// activate given view atleast
		setActs( data.acts[ 'view-source' ] || []);	// fill possible prepared sources
		
		function _cleanBord( e ) {
			while ( !!e && e.firstElementChild
			&& [ 'h1' ].indexOf( e.lastElementChild.id ) == -1
			&& e.lastElementChild.id
			&& e.lastElementChild.id.indexOf( 'view-' ) == -1
			&& e.lastElementChild.id.indexOf( 'column-' ) == -1
			&& e.lastElementChild.id.indexOf( 'type-' ) == -1 )
				DOM.remove( e.lastElementChild );
		}
	}
	, dropJson = function( s, e, p ) {
	//@ set viewData first and activate reverse to get first view as last activated
	var
		view = activeTool.sourceColumns ? activeView : 'view-source'
		, _data
		;
		try {
			if ( !/^[\{\["]/.test( s.trim()))
				_data = s.replace( /\n+/g, ' ' ).split( /\s*,\s*/gm );
			else
				_data = JSON.parse( s );
		}
		catch( e ) {
			console.warn( "JSON-Error or wrong format: "+ p + '!'	);
			return false;
		}
		if ( typeof _data.fileName == 'undefined' ) {
			if ( !_data.acts && useEngineer.getImport ) {
			// For long list of column orientated content it does not work, to append them
			// on source. Source may be overflow, so that table is not reachable to move
			// there. So the content must be appended to activeView to the already existing
			// entries, before the first bottom row. The id therefore has to be searched
			// and written into the first act: columnMove in import list (second entry).
			var
				id = view == activeView ? $( "table" ).querySelector( 'div.bottom' )	: false
				;
				_data.acts = {};
				id = !!id ? id.previousElementSibling : view == activeView
				? $( "table" ).lastElementChild : false;
				_data.acts[ view ] = useEngineer.getImport( joinCheck.myId, _data, types
				, subTypes );
				if ( !!id )				// append to last element before bottom
					_data.acts[ view ][ 1 ].data = id.id;
			}
			if ( !!_data.acts  ) {
				_setData( _data.acts );
				for ( var n in _data.acts )
					if ( data.acts[ n ])
						data.acts[ n ] = data.acts[ n ].concat( _data.acts[ n ]);
					else
						data.acts[ n ] = _data.acts[ n ];
				setAllViews( activeView, false );
				lastChange = 0;
				setNetworking( false );
			}
		}
		else if ( _data.toolType && _data.toolType != useEngineer.toolType
		|| !_data.groupName ) {
			createDialog( 'error', msgReplace( 'errorToolType'
			, _data.toolType || 'unbekannt/unknown' ));
			return true;
		}
		else {
			data = _data;
			idList = [];
			data.cryptStart = String.fromCharCode( data.cryptStart );
			_setData( data.acts );
			view = activeView;
			activeView = false;
			setAllViews( view );
			lastChange = 0;
			setNetworking( false );
		}
		return true;

		function _setData( acts ) {
		// replace wiki-Link with real links...
			if ( $CN.contains( D.body, 'lockedMode' ))
				acts = JSON.parse( JSON.stringify( acts ).replace( /\[\[([^\s]+)\s(.*?)\]\]/g
				, '<a href="$1" target="_blank">$2</a>' ));
			for ( var n in acts )
				acts[ n ].forEach(( a ) => {
					a.view = n;
					if ( a.act == 'viewActivate' )
						activeView = a.id;
					else if ( a.act.slice( -4 ) == 'Copy' ) {
						a.data = a.act.slice( 0, -4 ) +'-'+ a.data;
						idList.push( a.id );
					}
				});
		}
	}
	, setViewShot = function( id ) {
	// https://www.tutorialspoint.com/How-to-draw-an-SVG-file-on-an-HTML5-canvas
	//@ IMPORTANT: </br> kills svg!!
		if ( $( id ) && ( !isLocked || $CN.contains( D.body, 'lockedMode' ))) {
		var
			e = $( 'table' )
			, w = $I( e.offsetWidth / 4 )		// size it down to get better font cut...
			, h = $I( e.offsetHeight / 2 )	// take enough space, if view is longer...
			, img = $( id ).firstElementChild
			, DOMURL = W.URL || W.webkitURL || W	// do it the old way for safari...
			, s = $( 'localStyle' ).outerHTML
			, blob = new Blob([ 
	         '<svg xmlns="http://www.w3.org/2000/svg" width="'+ w +'" height="'+ h 
				+ '"><foreignObject width="100%" height="100%">'
				+ '<div xmlns="http://www.w3.org/1999/xhtml" '			// this is important!
				+ 'style="font-size:0.25em;background-color:white;">'
				+ '<style type="text/css">'	// get stylesheet (also ¼)
				+ globalStyle + s.slice( s.indexOf( '>' ) +1, -8 )
				+ '#table{overflow:hidden;}</style>'
				+ '<div class="'+ data.appearance +'">'	// set appearence class
				+ _escapeXML( e.outerHTML )	// get content, split not supported <br>
				+ '</div></div></foreignObject>'
				+ '</svg>'
			], { type: 'image/svg+xml' })
			;
         img.onload = function() {
            D.createElement( "canvas" ).getContext( '2d' ).drawImage( img, 0, 0 );
            DOMURL.revokeObjectURL( img.src );
         }
         img.src = DOMURL.createObjectURL( blob );
		}

		function _escapeXML( s ) {
		var
			_e = D.createElement( 'textarea' )
			;
			_e.innerHTML = s.replace( /<\/?br>/g, '\n'
			).replace( /&nbsp;/g, ' '			// well, some cases in chrome
			).replace( /\s(on[a-z]+|contenteditable|draggable)="[^"]+"/g, ''
			).replace( /[\n<>&'"\\]/g, function( c ) {
				switch( c ) {
					case '<': return '&lt;';
					case '>': return '&gt;';
					case '&': return '&amp;';
					case '\'': return '&apos;';
					case '"': return '&quot;';
					case '\n': return '&#xA;';
				}
			});
			return _e.childNodes.length === 0 ? "" : _e.childNodes[ 0 ].nodeValue;
		}
	}
	, initTest = function() {
		e = $( 'table' ).querySelectorAll( '.type.radio,.type.extend,.type.tab' );
		for ( var i = 0; i < e.length; i++ ) {
			if ( $CN.contains( e[ i ], 'radio' )) {
				if ( !$CN.contains( e[ i ].previousElementSibling, 'radio' )
				&& !$CN.contains( e[ i ].previousElementSibling, 'extend' ))
					$CN.add( e[ i ], 'checked' );
			}
			else if ( $A( e[ i ].lastElementChild, 'data' ).length )
				$CN.add( e[ i ], 'inactive' );
		}
	}
	, onTestClick = function( e ) {
		for ( var t in types )
			if ( $CN.contains( e, t ))
				break;
		if (( t == 'radio' || t == 'extend' )
		&& !$CN.contains( e, 'checked' ))
			_setRadio( e );
		else if ( t == 'checkbox' )
			$CN.toggle( e, 'checked' );
		if ( t == 'button' || t == 'tab' || t == 'extend' )
			activate( e, true );

		function _setRadio( _e ) {
			$CN.add( _e, 'checked' );
			while ( __check( _e = _e.previousElementSibling ))
				$CN.remove( _e, 'checked' );
			_e = e;
			while ( __check( _e = _e.nextElementSibling ))
				$CN.remove( _e, 'checked' );

			function __check( __e ) {
				return !!__e && ( $CN.contains( __e, 'radio' )
				|| $CN.contains( __e, 'extend' ));
			}
		}
	}
	, cryptLocalData = function( v, _data, EXTRACT ) {
		getHashes( "SHA-512", [ v[ 0 ] + testSalt ]
		, function( a ) {
			testHash = checkHash( a[ 0 ]) || str2cryptbase( str2url64( a[ 0 ]));
			if ( EXTRACT ) {
				activeCryptLength = 0;
				data.acts = {};
				a =  cryptOut( url642unicode( _data ), testHash );
				if ( a[ 0 ] == '{' )
					dropJson( a );
				else
					createDialog( 'error', msg.errorTest );
			}
			else
				storeSelf( unicode2url64( cryptIn( _data, testHash ))); 
		});
	}
	, storeSelf = function( jsonData ) {
	var
		s = ownHTML
		;
		if ( isLocked && $( 'lockLabel' ))
			s = s.replace( /(<body.+class="notActive)"/, '$1 lockedMode"' );
		storeIt( s.replace( /(\<!\--useAsApp)[\S\s]+(?=-->)/, "$1"
		+ ( jsonData && jsonData.length ? '\n'+ jsonData +'\n'
		: " /* dies durch JSON-Inhalt ersetzen ** replace this with JSON content */" ))
		, useEngineer.toolType, 'html' );
	}
	, doLocalStore = function() {
		createDialog( 'prompt', msg.localStore[ 0 ], [
			{ t: msg.localStore[ 1 ], v: 'includeData', type: 'checkbox' }
			, { t: msg.localStore[ 2 ], v: '', l: 192, type: 'password' }
		]
		, function( v ) {
			if ( v[ 0 ] == 'includeData') {
			var
				_data = useEngineer.storeJson( true )
				;
				for ( var n in _data.acts )
					_data.acts[ n ].forEach(( a ) => {
						delete a.view;
						if ( a.act.slice( -4 ) == 'Copy' )
							a.data = a.data.slice( a.act.length -3 ); 
					});
				_data = JSON.stringify( _data );
				if ( v[ 1 ].length )		// given password?
					cryptLocalData( [ v[ 1 ]], _data, false ); 
				else
					storeSelf( _data );
			}
			else
				storeSelf( false );
		});
	}
	, getXMLspreadsheet = function ( json, excelXML ) {
	//@ a short way to create a multiple sheets .xls file content (XML Spreadsheet)
	//@ see https://en.wikipedia.org/wiki/Microsoft_Excel#File_formats 
	//@ REMARK: all values are handled as strings (else not needed)
	var
		t = '<?xml version="1.0"?>\n<Workbook\n'
		+ 'xmlns="urn:schemas-microsoft-com:office:spreadsheet"\n'
		+ 'xmlns:o="urn:schemas-microsoft-com:office:office"\n'
		+ 'xmlns:x="urn:schemas-microsoft-com:office:excel"\n'
		+ 'xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"\n'
		+ 'xmlns:html="http://www.w3.org/TR/REC-html40">'
		+ ( excelXML && excelXML.style ? '<Styles>'+ excelXML.style +'</Styles>': '' )
		;
		for ( var name in json )
			t += _getSheet( name, _getTable( json[ name ]));
		return t +'\n</Workbook>';

		function _getSheet( name, table ) {
			return '\n<Worksheet ss:Name="'+ name +'">\n<Table>'
			+ ( excelXML && excelXML.column ? excelXML.column : '' ) + table
			+ '\n</Table>\n</Worksheet>';
		}
		function _getTable( o ) {
		var
			_t = __getRow( Object.keys( o[ 0 ]), ' ss:StyleID="firstLine"' )
			;
			for ( var i = 0; i < o.length; i++ )
				_t += __getRow( Object.values( o[ i ]));
			return _t;

			function __getRow( a, s ) {
			var
				b = '\n<Cell><Data ss:Type="String">'
				, e = '</Data></Cell>'
				;
				return ( '\n<Row'+ ( s || '' ) +'>'+ b + a.join( e + b ) + e +'\n</Row>'
				).replace( new RegExp( '<Data ss:Type="String"></Data>', 'gmi' ), '' );
			}
		}
	}
	, activeEdit = { start: false }
	, activeStep = { e: false, timeout: false, type: '' }
	, storeContent = ''
	, hasOpener = false
	, openerCheck
	;
	if ( !( 'reversed' in D.createElement( 'ol' )))
		createDialog( 'error', msg.noSupport );
	try {	hasOpener = W.opener && W.opener.useEngineer; } catch( e ){}
	if ( !hasOpener )
		D.addEventListener( "DOMContentLoaded", function() {
		// check if a given link to confirm moderation password
	   var
	   	given = location.href.match( /\?code=([^&]+)&email=(.+)$/ )
	      ;
	      if ( given ) {
				data.email = given[ 2 ];
	      	setTimeout( function() {
	      		useEngineer.initAccount( decodeURI( given[ 1 ]));
	      	}, 2000 );
	      }
	   }, false );

	async function getJavaScript( e ) {
	// quick & easy exchange of extern javascript file to intern to store as one file
	let f = await fetch( e.src )
		, t = await f.text()
		, s = e.outerHTML
		;
		t = t.replace( /(\*\s+)%%/
		, '$1'+ $A( D.querySelector( 'meta[name="description"]' ), 'content'));
		ownHTML = ownHTML.replace( s, s.replace( / src="[^"]+"/, ''	).slice( 0, -9 )
		+'\n'+ t +s.slice( -9 ));
	}
	/****************************************************************************/
	/***************************** Global definitions ***************************/
	/****************************************************************************/
	return {
		toolType: "usePrototype"
		, $:$,$I:$I,$A:$A,$CN:$CN,DOM:DOM
		, checkPassword: checkPassword
		, info: {}
		, calc: { onoff: function( a, e, cL ) {
			if ( $CN.contains( e, 'boxradio' ) && $CN.contains( e, 'on' )) {
				for ( var i = 0, l = e.parentNode.querySelectorAll( '.boxradio' )
				; i < l.length; i++ )
					if ( l[ i ] !== e )
						$CN.replace( l[ i ], 'on', 'off' );
			}
		}}
		, init: function( rand ) {
		var
			p = location.search
			, js = D.body.innerHTML.match( /<\!--useAsApp\s*([\S\s]+)\s*-->/im )
			, e = useEngineer.$("useEngineer")
			;
			_initOwnHTML();
		   $CN.remove( D.body, "notActive" );
			if ( W.fetch && e.src )
				getJavaScript( e );
			rand = rand.map( c => String.fromCharCode( c )).join( '' );
			cryptValidation = ","+ rand.slice( 0, 11 );
			groupSalt = ","+ rand.slice( 12, 23 );
			testSalt = ","+ rand.slice( 23, 35 );
			activeTool = toolTypes[ useEngineer.toolType ];
			data.groupName = activeTool.toolAct;
			data.toolType = useEngineer.toolType;
			actTypeDesc = actTypeDesc.replace( /%&%/g, activeTool.viewName ).split( ',' );
			msg.viewEdit = msg.viewEdit.replace( /%&%/g, activeTool.viewName );
			if ( hasOpener ) {
			var
				e = D.createElement( 'div' )
				;
				D.getElementsByTagName('title')[0].innerHTML += ' (privat)';			
				setDragDrop( e );
				e.id = "dropTouchSend";
				e.title = msg.privatework[ 3 ];
				DOM.insertBefore( $( 'source' ), e );
				nomix = createDialog( 'alert', msg.privatework[ 0 ] + ( DOM.isTouch
				? msg.privatework[ 1 ] : '' ));
				setTimeout( function(){ DOM.remove( nomix );}, 8000 );
				innerHTML.title = msg.privatework[ 2 ];
				useEngineer.privateTime = W.opener.useEngineer.privateTime
				= new Date().getTime();				// for check if parent window is reloaded
				W.opener.useEngineer.privateWork = window;
				useEngineer.joins = W.opener.useEngineer.joins
				$CN.add( $( 'privatework' ), 'inactive' );
				openerCheck = setInterval( function() {
					if ( !hasOpener || W.opener.closed
					|| W.opener.useEngineer.privateTime != useEngineer.privateTime ) {
						clearInterval( openerCheck );
	            	W.close();
	         	}
	         }, 2500 );
			}
			else
				innerHTML.title = data.title = titleDefault = _replaceType( titleDefault );
			title.addView = activeTool.viewName + title.addView;
			title.appearance = activeTool.appearance;
			title.export = activeTool.export;
			ariaLabel.views = activeTool.viewName;
			for ( var id in title ) {
				$A( $( id ), "title", title[ id ]);
				if ( calls[ id ])
					$A( $( id ), 'onclick', "return useEngineer."+ calls[ id ]);
			}
			for ( var id in innerHTML )
				( $( id ) || {}).innerHTML = innerHTML[ id ];
			for ( var id in ariaLabel )
				$A( $( id ), "aria-label", ariaLabel[ id ] );
			for ( n in msg )
				if ( n != 'titles' ) {
					if ( Array.isArray( msg[ n ]))
						msg[ n ][ 0 ] = _replaceType( msg[ n ][ 0 ]);
					else
						msg[ n ] = _replaceType( msg[ n ]);
				}

			setNetworking( false );
			joinCheck.getId();
			"table views source trash".split( ' ' ).forEach( e => setDragDrop( $( e )));
			// append types pending on toolType and drag'n drop level
			addTypes();
			if ( activeTool.views.length )
				activeTool.views.forEach( function( id ) {
					id = 'view-'+ id;
					viewCopy( id, id );
					data.acts[ id ] = [];
					activate( $( id ));
				});
			if ( $( 'recommend' ))
				checkSend( viewData, 'viewCopy', -1 );
			activeView = data.acts[ viewData ][ activeTool.views.length ].id;
			if ( !hasOpener && p.length > 2 * nameLength )
				_getGroupworkDialog();
			else if ( js )
				_getLocalStoreDialog();
			_setToggle( "copytoggle" );
			if ( !$CN.contains( D.body, 'lockedMode' ))
				_setToggle( "locktoggle" );
			else
				isLocked = true;
			lastSave = lastChange = 0;

			W.onbeforeunload = function() {	// very strange: caused on former misuse
				if ( hasOpener && !W.opener.closed )
					W.opener.useEngineer.privateWork = false;
				if( !reloadData && lastSave < lastChange ) return false; else return;
			};
			D.addEventListener( "keydown", function _keyDown( ev ) {
				if ( ev.keyCode === 112 ) {
					ev.preventDefault();
					useEngineer.showHelp();
				}
				else if (( ev.key == 's' || ev.key == 'S' ) && ( ev.ctrlKey || ev.metaKey )) {
            	ev.preventDefault();     
            	ev.stopPropagation();
            	doLocalStore();
            }
				else
					checkDialog( ev );
			});
			D.addEventListener( "click", function _docClick( ev ) {
				if ( isLocked && $( activeTool.sourceColumns ? 'views' : 'table'
				).contains( ev.target )) {
					ev.preventDefault();
					ev.stopPropagation();
					ev = ev.target;
					if ( activeTool.sourceColumns ) {
						ev = $CN.contains( ev, 'view' ) ? ev : ev.parentNode;
						if ( !$CN.contains( ev, 'active' ))
							activate( ev, false );
					}
					else
						onTestClick( $CN.contains( ev, 'type' ) ? ev : ev.parentNode );
				}
			});

			function _initOwnHTML() {
			var
				nomix = 'DOCTYPE HTML'
				, r = $( 'useEngineerStyle' )
				;
				ownHTML = '<!'+ nomix +'>\n'
				+ D.getElementsByTagName( 'html' )[ 0 ].outerHTML;
				if ( r && r.tagName == 'LINK' ) {
					r = r.sheet.cssRules
					for( var i = 0; i < r.length; globalStyle += r[ i++ ].cssText );
					ownHTML = ownHTML.replace( /<link[^>]+useEngineer\.css[^>]+>/
					, '<style id="useEngineerStyle" type="text/css">'
					+ globalStyle +'</style>' );
				}
				else
					globalStyle = r.innerHTML;
			}
			function _setToggle( t ) {
				if ( t = $( t )) {
					t.checked = false;
					$A( t, 'onchange', "useEngineer.toggleMode(this)" );
				}
			}
			function _replaceType( s ) {
				return s.replace( /%\$%/g, useEngineer.toolType
				).replace( /%§%/g, activeTool.toolAct );
			}
			function _getLocalStoreDialog() {
				if (( js = js ? js[ 1 ].trim() : '').length && !/^\/\*/.test( js )) {
					if ( js.length && js[ 0 ] == '{' )
						dropJson( js );
					else {
						createDialog( 'prompt', msg.localDecrypt[ 0 ]
						, [{ t: msg.localDecrypt[ 1 ], v: '', l: 192, type: 'password' }]
						, function( v ) {	cryptLocalData( v, js, true ); }
						);
					}
				}
			}
			function _getGroupworkDialog() {
				createDialog( 'prompt', msg.getInitPassword[ 0 ], [
					{ t: msg.getInitPassword[ 1 ], v: '', l: 192, type: 'password' }
				], function( v ) {
					getHashes( "SHA-512", [ v[ 0 ] + groupSalt ]
					, function( a ) {
					initHash = checkHash( a[ 0 ]) || str2cryptbase( str2url64( a[ 0 ]));
					var
						crypt = cryptOut( url642unicode( p.slice( 4 )), initHash )
						;
						data.grouppassword = cryptbase2str( initHash );
						data.fileName = crypt.slice( 0, nameLength );
						crypt = crypt.slice( nameLength +4 );
						if ( !/[^A-Z0-9._-]/i.test( data.fileName )
						&& !/[^A-Z0-9_-]/i.test( crypt.slice( 1 ))) {
							data.cryptStart = crypt[ 0 ];
							data.cryptBase = str2cryptbase( crypt.slice( 1 ));
							loadFullFile();
						}
						else 
							createDialog( 'error', msg.errorLink );
					});
				});
			}
		}
		, showHelp: function() {
			createDialog( 'help', this.helpText
			+ msg.helpText.replace( /%%/, this.helpExtend ));
		}
		, storeJson: function( GETDATA = false ) {
		var
			_data = JSON.parse( JSON.stringify( data ))
			, ids = []
			, id = 'view-source'
			, newActive = false
			;
			_data.cryptStart = !_data.cryptStart || _data.cryptStart.charCodeAt( 0 );
			_data.acts = {};
			_data.acts[ viewData ] = [];
			_data.acts[ id ] = [];
			if ( data.acts[ id ]) {
				if ( activeTool.sourceColumns )
					_data.acts[ id ] = getViewActs( data.acts[ id ], id, ids );
				else
					_data.acts[ id ] = getSourceActs( data.acts[ id ], id, ids );
			}
			for ( var i = 0, eL = $( "views" ).children, a = []; i < eL.length; i++ ) {
				if ( !/^view-/.test( eL[ i ].id )) {
					id = getId( ids );
					ids.push( id );
					a.push( getAct( "viewCopy", viewData, viewData, id ));
					a.push( getAct( "viewEdit", viewData, $A( eL[ i ], 'data' ), id ));
					if ( $A( eL[ i ], 'id' ) == activeView )
						newActive = id;
					if ( data.acts[ eL[ i ].id ]) {
						_data.acts[ id ] = getViewActs( data.acts[ eL[ i ].id ], id, ids );
						ids = ids.concat( _data.acts[ id ].idList );
						delete _data.acts[ id ].idList;
					}
				}
			}
			// append activeView with its new id
			a.push( getAct( 'viewActivate', viewData, getRandId(), newActive ));
			_data.acts[ viewData ] = a;
			if ( !!GETDATA )
				return _data;
			else {
				for ( var n in _data.acts )
					_data.acts[ n ].forEach(( a ) => {
						delete a.view;
						if ( a.act.slice( -4 ) == 'Copy' )
							a.data = a.data.slice( a.act.length -3 ); 
					});
				storeIt( JSON.stringify( _data, null, '\t' ), data.groupName, 'json' );
				lastSave = new Date();
			}
			return false;
		}
		, storeExport: function() {
		var
			_data = useEngineer.storeJson( true )
			, fN = _data.title.replace( /[^a-z_-]/ig, '' ) +'Export'
			, storeContent = useEngineer.getExport(
				_data.acts, activeTool
				, _data.title
				, types
				, steps
			)
			;
			if ( !!activeTool.excelXML )
				storeIt( getXMLspreadsheet( storeContent, activeTool.excelXML ), fN, 'xls' );
			else if ( storeContent.length ) {
				createDialog( 'confirm', msg.storeExport
				+ '<pre style="text-align:left;font-size:0.9em"><p>'
				+ storeContent +'</pre>', 0, function( CONFIRMED ) {
					if( CONFIRMED )
						storeIt( storeContent, fN, 'txt' );
				});
			}
			else
				createDialog( 'error', msg.noExport );
			return false;
		}
		, openPrivateWork: function(ev) {
		//@PARAMETER headerHTMLContent, bodyHTMLContent, className
			ev.preventDefault();
			if( !$CN.contains( $( 'privatework' ), 'inactive' )) {
				if ( !!useEngineer.privateWork )
					useEngineer.privateWork.focus();
				else
					W.open( location.pathname, "_blank", 'popup=1,rel=opener,'
					+ 'width=850,left=100,dependent=yes,scrollbars=yes,resizable=yes' );
			}
		}
		, initAccount: function( code="" ) {
			createDialog( 'prompt', msg.initAccount[ 0 ], [
				{ t: msg.initAccount[ 1 ], v: data.email || '', type: 'email' }
				, { t: msg.initAccount[ 2 ], v: code || '', l: 26, type: 'text' }
				, { t: msg.initAccount[ 3 ], v: '', l: 192, type: 'password' }
				], function( v ) {
				getHashes( "SHA-512", [ v[ 0 ] + emailSalt, v[ 1 ].replace( /["\s]/g, '' )
				, v[ 2 ] + globalSalt ]
				, function( a ) {
					getAjax(); // reset period
					ajax( 'POST', path +'exchange/password.php?lg='+ language.slice( 1 )
					+ '&to='+ useEngineer.toolType + '&id='+ str2url64( a[ 0 ]) 
					+ '&co='+ str2url64( a[ 1 ]) +'&pw='+ str2url64( a[ 2 ]), null
					, function( t ) {
						t = t !== false;
						createDialog( t ? 'success' : 'error', msgReplace( t ? 'newPassword'
						: 'noAccount', v[ 0 ]));
					}, 8000 );
				});
			});	
		}
		, getPassword: function() {
		var
			email = $( $( "prompt0" ).type == 'email' ? "prompt0" : "prompt1" ).value
			;
			createDialog( 'prompt', msg.getPassword[ 0 ], [
				{ t: msg.getPassword[ 1 ], v: email, type: 'email' }
			], function( v ) {
				getHashes( "SHA-512", [ v[ 0 ] + emailSalt ], function( a ) {
					ajax( 'POST', path +'exchange/password.php?lg='+ language.slice( 1 )
					+ '&to='+ useEngineer.toolType +'&id='+ str2url64( a[ 0 ])
					+ '&em='+ v[ 0 ], null, function( t ) {
						t = t !== false;
						createDialog( t ? 'success' : 'error', msgReplace( t
						? 'sendIdent' : 'noAccount', v[ 0 ]));
					}, 8000 );
				});
			});
		}
		, addGroup: function() {
		var
			preDef = data.fileName && data.cryptBase && data.cryptStart
			;
			createDialog( 'prompt', msg.addGroup[ 0 ], [
				{ t: msg.addGroup[ 1 ], v: data.title || titleDefault, l: 299 }
				, { t: msg.addGroup[ 2 ], v: data.email || emailDefault, type: 'email' }
				, { t: msg.addGroup[ 3 ], v: '', l: 192, type: 'password' }
				, { t: msg.addGroup[ 4 ], v: 'newLink', type: 'checkbox', attr: 
				( preDef ? ' onchange="this.parentNode.parentNode.nextElementSibling.style='
				+ '\'display:\'+(this.checked?\'block\':\'none\')"'
				: ' disabled="disabled" checked="checked"' )}
				, { t: msg.addGroup[ 5 ] + ( preDef ? msg.addGroup[ 6 ] : '' )
				, v: preDef ? '_receiveOld_' : '', l: 192, type: 'password'
				, style: preDef ? 'display:none;' : false }
			], function( v ) {
				_addGroup( v[ 0 ], v[ 1 ], v[ 2 ], v[ 3 ], v[ 4 ])
			});
			return false;

			function _addGroup( title, email, mainpasswort, newLink, grouppassword ) {
				if ( email == emailDefault || mainpasswort.length ) {
				var
					head = getRandomUnicode( getRandom16( 1 ) % 128 ) +','	// 4.secret
					+ title +','+ data.appearance
					, oldJoin = joinCheck.myJoin
					, waitMsg = false
					, _data
					;
					newLink = newLink == 'newLink';
					email = email.toLowerCase();
					head = String.fromCharCode( head.length ) + head;
					joinCheck.getId();
					_data = useEngineer.storeJson( true );
					head += sendActs( joinCheck.getJoins(), true );
					head += sendActs([ getAct( "viewEdit", viewData, cryptValidation
					, viewData )], true );
					for ( var n in _data.acts )					// append active acts...
						head += sendActs( _data.acts[ n ], true );
					// append viewActivate with new unknown id to detect reload of data
					mainpasswort = [ email + emailSalt, ( mainpasswort || '123' )
					+ globalSalt ];
					if ( grouppassword )
						mainpasswort.push( grouppassword + groupSalt );
					if ( networking )	// tell possible active sessions to stop for reloading
					// if viewReload is found some where in loaded data a reload is started
						sendActs([ getAct( "viewReload", viewData, getRandId())]);
					reloadData = true;
					activeCryptLength = 0;
					getHashes( "SHA-512", mainpasswort, function( a ) {
					var
						l = 0
						, stop
						;
						if ( newLink || !data.cryptBase || !data.cryptStart ) {
							data.email = email;
							data.title = title;
							data.cryptBase = getRandom16( cryptLength );		// 2.secret
							l = getRandom16( 1 ) % head.length;					// 3.secret
							data.cryptStart = String.fromCharCode( l +2 );
							if ( grouppassword && grouppassword != '_receiveOld_' )
								data.grouppassword = str2url64( a[ 2 ]);
							newLink = true;
						}
						else
							l = data.cryptStart.charCodeAt( 0 ) -2;
						waitMsg = createDialog( 'alert', msg.reloadWait + ( newLink
						? msg.newGroupLink : '' ));
						waitMsg.firstChild.firstChild.style.width = '30em';
						setTimeout( function() { DOM.remove( waitMsg ); }, 4500 );
						if ( newLink || !data.fileName )
							data.fileName = cryptbase2str( getRandom16( 33 )).slice( -identLen );
						initHash = str2cryptbase( data.grouppassword );
						head = [	// to beware the 3.secret on writing the data in the first
						// step:	split on cryptStart. Attention: cryptIn adds 2 signs in front
						cryptIn( head.slice( 0, l ))
						// ...and cut cryptStart from beginning of head
						, cryptIn( head.slice( l ))
						].join( '' ).slice( 1 );	
						// second step after waiting four seconds rewrite exchange file...
						setTimeout( function() {	// wait for reload...
							ajax( 'POST', path +'exchange/exchange.php?em='
							// email && globalPassword && groupPassword
							+ str2url64( a[ 0 ]) +'&pw='+ str2url64( a[ 1 ])
							+ '&gw='+ data.grouppassword
							, data.fileName + head
							, function( t ) {
								if ( t !== false ) {
									data.fileName = t;
									createEmail( msg.emailDialog, msg.emailSubject, 'emailBody'
									, path + useEngineer.toolType +'-APP'+ language +'.html?uC='
									+ unicode2url64( cryptIn(
											data.fileName + '.txt'
											+ data.cryptStart + cryptbase2str( data.cryptBase )
										, initHash ))
									);
									setNetworking( true );
									reloadData = false;
									activeCryptLength = 0;
									loadFullFile( true );
									if ( newLink )
										useEngineer.storeJson();
								}
								else {
									joinCheck.myJoin = oldJoin;
									joinCheck.myid = oldJoin.split( '-' )[ 0 ];
									createDialog( 'error', msg.noAddGroup );
								}
							}, 5500 );
						}, 3000 );	// end wait for reload
					});
				}
				else
					createDialog( 'error', msg.noAddGroup );
			}
		}
		, step: function( ev, type ) {
		var
			e = ev.currentTarget
			, v
			;
			if ( !isLocked ) {
				e = e.parentNode;
				if ( e.parentNode.id != 'source' || activeTool.sourceStore ) {
					if ( activeStep.e && e != activeStep.e )
						_send();
					ev.stopPropagation();
					activeId = e.id;
					activeStep.e = e;
					activeStep.type = type;
					activeStep.v = setClass( e, type );
					clearTimeout( activeStep.timeout );
					activeStep.timeout = setTimeout( _send, 1000 );
				}
			}

			function _send() {
				checkSend( $( 'source' ).contains( activeStep.e ) ? 'view-source'
				: activeView, $CN.contains( activeStep.e, 'column' ) ? 'columnSize'
				: activeStep.type, activeStep.v );
				activeStep.e = false;
			}
		}
		, select: function( ev ) {
		var
			e = ev.currentTarget
			;
			if ( !isLocked ) {
				ev.stopPropagation();
				activeId = e.parentNode.parentNode.id;
				checkSend( activeView, 'attr', e.value );
			}
		}
		, edit: function( ev, type ) {
		var
			e = ev.currentTarget
			;
			if ( !isLocked ) {
				ev.stopPropagation();
				activeId = e.parentNode.id;
				activeEdit.id = activeId;
				activeEdit.type = type = type == 'choise' ? 'choiseEdit' : 'targetEdit';
				activeEdit.start = ( $CN.contains( e, 'multi' ) ? '+ '
				: $CN.contains( e, 'single' ) ? '- ' : '' )
				+ ( $A( e, 'data' ) || '' );
				activeEdit.active = activeEdit.start;
				createDialog( 'prompt', msg[ type ]
					, [{ v: activeEdit.start, l: type == 'targetEdit' ? 50 : 350 }]
					, useEngineer.sendEdit
				);
			}
			else if ( $CN.contains( e, 'toggle' ))
				$CN.toggle( e, 'active' );
		}
		, click: function( ev ) {
		var
			t = ev.target.id.length ? ev.target : ev.target.parentNode
			, type
			;
			if ( !isLocked ) {
				ev.stopPropagation();
				ev.preventDefault();
				if ( t.parentNode.id != 'source' ) {
					activeId = t.id;
					type = /view-/.test( t.id ) ? t.id.slice( 5 ) : false;
					if ( t.tagName != 'SELECT' && t.tagName != 'OPTION'
					&& t.lastElementChild.tagName == 'IMG' ) {	// it's a click on view
					var
						r = DOM.getRect( t )
						;
						if ( DOM.getCoord( ev ).y < r.y + ( 0.75 * r.h )
						&& activeView != t.id ) {
							checkSend( t.id, 'viewActivate', t.id );
							return false;
						}
						else if ( !type || !views[ type ] || !views[ type ].notEdit ) {
							activeEdit.id = activeId;
							activeEdit.type = 'viewEdit';
							activeEdit.start = $A( t, 'data' ) || '';
							createDialog( 'prompt', msg.viewEdit, [{ v: activeEdit.start
							, l: 50 }], useEngineer.sendEdit );
						}
					}
				}
			}
			return false;
		}
		, setEditable: function( ev, SOURCE ) {
		// to prevent chrome from 'edit' all the time
			ev = ev.currentTarget;
			if ( !$( 'source' ).contains( ev ) || SOURCE == 'all' // also editable in source
			|| SOURCE == 'some' && ev.parentNode.id.indexOf( 'type-' ) < 0 )
				$A( ev, 'contentEditable', 'true' );
		}
		, typeEdit: function( ev, phase ) {
		var
			e = ev.currentTarget
			;
			activeEdit.type = 'typeEdit';
			if ( isLocked )
				e.blur();
			else if ( phase == 'focus' ) {
				if ( !!activeEdit.start && activeEdit.id != e.parentNode.id )
					e.blur();			// other element active?!
				else {
					activeId = e.parentNode.id;
					activeEdit.id = activeId;
					$CN.add( e.parentNode, 'quitEdit' );
					if( activeEdit.draggable = $A( e.parentNode, 'draggable' ))
						$A( e.parentNode, 'draggable', -1 );
					if ( !activeEdit.start )
						activeEdit.active = activeEdit.start = cleanEdit( e );
				}
			}
			else if ( phase == 'input' ) {
				activeEdit.active = cleanEdit( e );}
			else if ( phase == 'blur' ) {
				if( activeEdit.draggable )
					$A( e.parentNode, 'draggable', "true" );
				$CN.remove( e.parentNode, 'quitEdit' );
				$CN.remove( e.parentNode, 'doubleEdit' );
				$A( e, 'contentEditable', -1 );
				if ( !!activeEdit.active && activeEdit.active != activeEdit.start )
			 		useEngineer.sendEdit([ activeEdit.active ] );
			 	else
			 		activeEdit.start = false;
			}
		}
		, sendEdit: function( v ) {
			if ( v[ 0 ] != activeEdit.start && activeEdit.id == activeId ) {
				if ( !!networking ) 	// wait for new data available load them...
					getAjax.wait = function() { _sendEdit( v ); };
				else
					_sendEdit( v );
			}
			else		// clean onblur
				activeEdit = { start: false };	

			function _sendEdit( v, TYPEEDIT ) {
			var
				act = activeEdit.type
				;
				// standardize choise
				v = act == 'choiseEdit' ? v[ 0 ].replace( /^\s*(\+|-)\s*/, '$1 '
				).split( /,\s*\n\s*|\s*,\s*|\s*\n\s*/	).join( '\n' ) : v[ 0 ];
				if ( !!activeEdit.doubleEdit ) {
					if ( v != activeEdit.doubleEdit )	// only act, if different
				 		createDialog( 'confirm', msgReplace( 'editParallel', v
				 		).replace( /\$\$/, activeEdit.doubleEdit )
				 		+ msg[ act == 'choiseEdit' || act == 'typeEdit' ? 'editTypeCopy'
				 		: 'editOverwrite' ], 0, __set );
				 	else
				 		__set( false );
				}
				else
					__set( true );

				function __set( CONFIRMED ) {
				var
					act = activeEdit.type
					, sourceType = act == 'typeEdit' && activeId.indexOf( 'type-' ) > -1
					? activeEdit.start : false
					, view = act == 'viewEdit' ? viewData
					: $( 'source' ).contains( $( activeId )) ? 'view-source' : activeView
					, acts = []
					;
					activeEdit.last = activeEdit.start;
					activeEdit.start = false;
					if ( CONFIRMED ) {
						act = act == 'choiseEdit' || act == 'targetEdit' ? 'attr' : act;
						// copy active and set value there
						if ( sourceType ) {
							$( activeId ).querySelector( '.content' ).innerHTML = sourceType;
							acts = checkSend( view, 'typeCopy'
							, $( 'source' ).lastElementChild.id, true );
							acts.push( getAct( act, view, v, acts[ 0 ].id ));
						}
						else if ( !!activeEdit.doubleEdit
						&& ( activeEdit.type == 'choiseEdit' || act == 'typeEdit' )) {
							setActs([ getAct( act, view, activeEdit.doubleEdit, activeId )]);
							// get copy of element...
							acts = checkSend( view, 'typeCopy', activeId, true );
							// ... new created id to change content
							acts.push( getAct( act, view, v, acts[ 0 ].id ));
						}
						else
							acts.push( getAct( act, view, v, activeId ));
						sendActs( acts );
					}
					else if ( !!activeEdit.doubleEdit )
						setActs([ getAct( act, view, activeEdit.doubleEdit, activeId )]);
					activeId = false;
				}
			}
		}
		, sendEditDirect: function( id, v ) {
			sendActs([ getAct( 'typeEdit', activeView, v, id )]);
		}
		, allow: function( ev ) { ev.preventDefault(); return true; }
		, forbit:  function( ev ) { ev.preventDefault(); return false; }
		, dragstart: function( ev ) {
		var
			e = ev.currentTarget
			, view = $( 'source' ).contains( e ) ? 'view-source' : activeView
			;
			e = e.id.length ? e : e.parentNode;
			activeId = e.id;
			if (( !!useEngineer.privateWork || ( hasOpener && !W.opener.closed ))
			&& (data.acts[ activeView ] && data.acts[ view ].length )) {
			var
				type = getType( e )
				, acts = type == 'view' ? data.acts[ e.id ] : data.acts[ view ];
				;
				if ( type == 'type' )
					acts = getActList( acts, view, e.id, 'none', e.id );
				else if ( type == 'column' )
					acts = getColumnActs( acts, view, e.id, 'none', e.id );
				else {
					acts = getViewActs( acts, e.id );
					acts.push( getAct( "viewActivate", viewData, getRandId(), e.id ));
					acts.unshift( getAct( "viewEdit", viewData, $A( e, 'data' ), e.id ));
					acts.unshift( getAct( "viewCopy", viewData, viewData, e.id ));
				}
				e = JSON.stringify({
					private: !!hasOpener
					, type: type
					, acts: acts
				});
				if ( ev.dataTransfer )
					ev.dataTransfer.setData( "text/plain", e );
				else		// well, workaround for IOS
					useEngineer.dataTransfer = e;
			}
			return true;
		}
		, dropTouchSend: function( privTrans, tType ) {
			sendDropList( privTrans
			, tType == 'view' ? viewData : tType == 'type' ? 'view-source' : activeView
			, $( tType == 'view' ? 'views' : tType == 'type' ? 'source' : 'table'
			).lastElementChild
			, tType );
		}
		, drop: function( ev ) {
		var
			f = ev.dataTransfer ? ( ev.dataTransfer.files || '' ) : ''
			;
			ev.preventDefault();
			ev.stopPropagation();
			if ( f.length && !isLocked ) // file is droped...
				this.dropfile( ev, f[ 0 ]);
			else {
			var
				iosTrans = (( this.privateWork || W.opener || {} ).useEngineer ) || {}
				, privTrans = ev.dataTransfer ? ev.dataTransfer.getData( "text" )
				: useEngineer.dataTransfer || iosTrans.dataTransfer || false
				, t = ev.target.id.length ? ev.target : ev.target.parentNode
				, tType = getType( t )
				, d = DOM.getCoord( ev )					// tribute to iOS
				, eType
				, view
				;
				f = !!privTrans ? privTrans.match( /"view":"([\d-]+)"/ ) : false;
				f = f ? !D.body.contains( $( f[ 1 ])) : false;
				eType = f ? privTrans.match( /"type":"([^"]+)"/i )[ 1 ]
				: getType( $( activeId ));
				iosTrans.dataTransfer = false;
				view = eType == 'view' ? viewData : $( 'source' ).contains( t )
				? 'view-source' : activeView;
				if ( t.id == "dropTouchSend" && !!hasOpener )
					W.opener.useEngineer.dropTouchSend( privTrans, eType );
				else if ( f ) {
					if ( tType != 'trash' ) {
						_correctPos();
						sendDropList( privTrans, view, t, tType );
					}
				}
				else if ( activeId && !isLocked ) { // if no file is droped...
				var
					e = $( activeId )
					, eChild = $( 'views' ).contains( e ) ? 'views'
					: $( 'table' ).contains( e ) ? 'table' : 'source' 
					;
					if ( !!tType ) {
						if ( tType == 'trash' ) {			// don't delete last ones...
							if (( eType == 'type' && e.id.indexOf( 'type' ) < 0 )
							|| ( eType == 'view'	&& e.id.indexOf( 'view' ) < 0
							&& $( 'views' ).children.length > 1 + activeTool.views.length )
							|| ( eType == 'column'
							&& ( eChild == 'source' && e.id.indexOf( 'column' ) < 0 )
							|| ( eChild == 'table' && $( 'table' ).children.length > 1 ))) {
								if ( eType == 'view' )
									createDialog( 'confirm', msgReplace( 'deleteConfirm'
									, $A( e, 'data' )), 0, function( CONFIRMED ) {
										if( CONFIRMED )
											checkSend( view, eType +'Delete', getRandId());
									});
								else
									checkSend( eChild == 'source' ? 'view-source' : view
									, eType +'Delete', getRandId());
							}
							return;
						}
						else if (( t == e && !$( "copytoggle" ).checked )
						|| (( eType != 'view' && ( tType == 'view' || tType == 'views' ))
						|| ( eType == 'type' && tType == 'table' )))
							return;
						else if ( view != 'view-source' || ( !!activeTool.sourceStore
						&& ( eType == 'type' && !activeTool.sourceColumns )
						|| ( eType == 'column' && !!activeTool.sourceColumns ))) {
							_correctPos();
							checkSend( view, eType + 'Move', tType == 'table'
							? 'column-'+ activeTool.col : t.id );
						}
					}
				}
			}

			function _correctPos() {
				if ( eType == 'column' && tType == 'type' ) {
				// only drop columns on columns
					t = t.parentNode;
					tType = 'column';
				}
				if ( eType == 'view' && tType != 'view' ) {
					t = $( activeView );
					tType = 'view';
				}
				else if ( tType != 'type' || eType == 'type' )
					__checkAllPos();
				else if ( t.id.indexOf( 'type-' ) > -1 ) 
					t = $( 'source' ).lastElementChild;

				function __checkAllPos() {
					if ( tType == 'source' || tType == 'table' || tType == 'views' ) {
						if ( t.lastElementChild ) {
							t = t.lastElementChild;
							if ( tType == 'table' && eType == 'column' ) {
								tType = getType( t );
								_checkAllPos();
							}
							else
								tType = getType( t );
						}
					}
					else if ( tType == 'view' || tType == 'type' ) {
						if ( d.y < t.getBoundingClientRect().top + t.offsetHeight / 2 ) {
							t = t.previousElementSibling
							&& !$CN.contains( t.previousElementSibling, 'size' )
							? t.previousElementSibling : t.parentNode;
							tType = getType( t ); // tType could be changed...
						}
					}
					else if ( eType == 'column' && tType == 'column' ) {
						if ( $CN.contains( t, 'top' ))
							___find( 'top', 'next' );
						else if ( $CN.contains( t, 'bottom' ))
							___find( 'bottom', 'previous' );
						else if( $CN.contains( t, 'p100' ))
							t = d.y < t.getBoundingClientRect().top + t.offsetHeight / 2
							?  t.previousElementSibling || t.parentNode : t;
						else
							t = d.x < t.getBoundingClientRect().left + t.offsetWidth / 2
							? t.previousElementSibling || t.parentNode : t;
						tType = getType( t );
					}
					// last: type on column...
					else if ( t.lastElementChild && !$CN.contains( t.lastElementChild, 'size' )) {
						for ( var i = 1, _t = false; i < t.children.length; i++ ) {
							if ( t.children[ i ].getBoundingClientRect().top > d.y ) {
								_t = t.children[ i -1 ];
								break;
							}
						}
						t = _t || t.lastElementChild;
						tType = getType( t );
					}

					function ___find( type, dir ) {
						dir += 'ElementSibling';
						while( t && t[ dir ] && $CN.contains( t, type ))
							t = t[ dir ];				
					}
				}
			}
		}
		, dropfile: function( ev, f ) {
			ev.stopPropagation();
			ev.preventDefault();
			f = !!f ? f : ev.dataTransfer.files[ 0 ];
			if ( !!f ) {
				ev = ev.currentTarget;		// event owning object
				if ( f.type.indexOf( 'json' ) == -1 )
					createDialog( 'error', msg.dropJson );
				else if ( networking )
					createDialog( 'confirm', msg.dropNetworking + ( DOM.isTouch
					? msg.privatework[ 1 ] : '' ), 0, function( CONFIRMED ) {
						if( CONFIRMED ) {
							setNetworking( false );
							_dropfile();
						}
					});
				else 
					_dropfile();
			}

			function _dropfile() {
			var
				r = new FileReader()
				;
				r.onload = function( n ) { return function( _ev ) {
					if ( !dropJson( _ev.currentTarget.result, ev, n )) 
						createDialog( 'error', msgReplace( 'dropImport', n ));
				}}( f.name );
				r.readAsText( f );	// start reading the file data.
			}
		}
		, addViewCol: function( e ) {
			if ( !isLocked )
				checkSend( activeView, e.id.slice( 3 ).toLowerCase() +'Copy', -1 );
		}
		, zoom: function( e ) {
		var
			v = $I(( parseFloat( $( 'table' ).style.fontSize ) || 1. ) * 10 )
			;
			v = Math.min( 14, Math.max( 4, v + ( e.id == "zoomin" ? -1 : 1 )
			))/ 10 ;
			$( 'table' ).style.fontSize = v +'em';
		}
		, setAppearance: function( v ) {
			v = v !== false ? v : data.appearance.length ? '' : 'touch';
			$CN[ v == 'touch' ? 'add' : 'remove' ]( D.body, 'touch' );
			data.appearance = v;
		}
		, toggleMode: function( e ) {
		var
			type = e.checked ? 'add' : 'remove'
			, cT = e.id == "copytoggle"
			;
			$CN[  cT ? type : 'remove' ]( $( 'table' ).parentNode, 'copy' );
			$CN[ !cT ? type : 'remove' ]( D.body, 'testing' );
			( $( cT ? "locktoggle" : "copytoggle" ) || {}).checked = false;
			if ( !cT && type == 'add' ) {
				isLocked = true;
				createDialog( 'help', this.lockOpener );
				if ( $( "testLabel" ))
					initTest();
			}
			else
				isLocked = false;
		}
		, doLocalStore: doLocalStore
		, getNumberCrypt: function() {
		var
			hash = isLocked ? testHash : initHash
			;
			if ( hash )
				createDialog( 'alert', msgReplace( 'numberCrypt', hash ));
			else
				createDialog( 'error', msg.noHash );
		}
		, sendRecommendation: function() {
			createEmail( msg.recommendDialog, msg.recommendSubject, 'recommendBody'
			, path + useEngineer.toolType +'-APP'+ language +'.html' );
		}
		, openHome: function() { W.open( homepage, "_blank"); }
		, joins: joinCheck.joins
		, getInitcode: function() { return '['+ getRandom16( 3 * 12 ).join(',') +']'; }
	}
})( document, window );