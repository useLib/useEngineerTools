<!DOCTYPE HTML>
<!--
	useProcessMap: a usability optimized prototyping Tool
	Copyright (C) 2022 Dr. Dirk Fischer, use-Optimierung, cologne

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
-->
<html lang="en-US">
<head>

<!-- technical metaTags -->
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<meta name="language" content="en">
<!-- content metaTags -->
<title>useEngineer's prozess mapping tool</title>
<link rel="icon" href="https://useengineer.com/use-favicon.ico" type="image/ico">
<meta name="author" content="Dr.-Ing. D. Fischer, use-Optimierung, Cologne">
<meta name="description" content="useProcessMap: a process description tool for the quality standard-compliant documentation of work processes">
<meta name="keywords" lang="en" content="process analyze, group work, tool, work system
, standardize, usability, ergonomics, systems engineering, interaktion optimization">
<meta name="keywords" lang="de" content="Prozessanalyse, Gruppenarbeit, Werkzeug
, Arbeitssystem, Standardisierung, Software-Ergonomie, Arbeitsgestaltung
, Interaktions-Optimierung">

<script id="useEngineer" src="useEngineer.en.min.js" type="text/javascript"></script>

<script id="uLToolcall" type="text/javascript">

	useEngineer.toolType = "useProcessMap";
	useEngineer.helpText = `<h1>Instructions for the useProcessMap-APP</h1>
	<p>The specific description of the <b>process steps and tasks</b> in useProcessMap can be used as a central part of a quality manual. It is used as a process description tool for documenting work processes in accordance with quality standards (ISO 9001/14001). Usability engineering to optimize usability is not possible without analyzing the embedding of work tasks in company processes or computer-aided work processes. Through <b>group work</b> with users and/or experts, <b>processes</b> and <b>work tasks</b> should be ordered and mapped <b>as completely</b> as possible. Human <b><a href="https://useengineer.com/download/C.1.2.Process Optimization.pptx.pdf" target="_blank"> usable processes</a></b> can only be developed, if the human actions are depicted in a way that is comprehensible to them. Process steps, workflows and/or subtasks coordinated in this way minimize the training period and, above all, errors. This also massively increases acceptance.</p>
	<p><em>useProcessMap</em> can be <b>used</b> with a wide range. Employees can orientate themselves on this, above all during the training period and in substitution phases. The rough orientation of necessary work steps up to the description of individual interactions in the user interface can be documented here and lead to a deeper understanding.</p>
	<p>All transactions and links sent are <b>high-quality encrypted</b> and protected by the assigned <b>passwords</b>. The following options are shown, which are based on <b>drag & drop</b> and <b>click</b>.</p>
	<h2>Direct Interactions</h2>
	<ul>
	<li>Like moderation cards, the process steps can be <b>free</b> moved to rows and storage areas or deleted again. With touch surfaces only <b><u>one</u> fingers</b> (several for scrolling and zooming). When you let go of the mouse pointer, they are sorted into the appropriate place. Lines are grabbed at their left side area.</li>
	<li>A <b>new</b> step is created by moving or changing the template.</li>
	<li>In <b>copy mode</b>, lines and processes can be copied <b>by moving</b>.</li>
	<li>If <b>fixing mode is activated</b>, the content can no longer be edited. If the active data is <b>using the button: &#x1F4CB; stored locally</b>, the content can no longer be changed.</li>
	<li>By clicking on the different areas of a process step, its <b>hierarchy</b> and the <b>ripeness levels</b> can be changed step by step for each regulation step.</li>
	<li>By clicking on the remaining area of a process step, the <b>content</b> can be changed.</li>
	<li>All changes are <b>immediately</b> applied to the display windows of all group members <b></b>.</li>
	<li>Of course, a process step can also be used for a <b>short message</b> to group members. However, it is always advisable to exchange information via an additional communication channel, e.g. Teams.</li>
	</ul>`;
	useEngineer.helpExtend = `&#x1F4F7;-button, the <b>appearance</b> can be adjusted, for example for documentation purposes, so that only the <b>active process</b> is displayed, with no interaction area: trash, supply and process selection.`;

	useEngineer.info.info = `<p><b>Work processes and tasks</b> require complete control loops and <b>action&shy;regulation&shy;cycles</b>, i.e. all four PDCA steps. These are defined here and can be broken down in detail (indented) as required.</p>
	<p>It is possible to store <b>references or links</b> to records and documents. For simplification, an entry analogous to the wiki standard is provided in <b>square brackets</b> [[this/is/the/path with description]].</p>`;

	// <colgroup><col><col></colgroup> notworking on svg foreignobject for image!
	useEngineer.info.overview = `<h1>Process overview with task assignment</h1>
	<p>The quality standards according to DIN ISO 9001/14001 provide for clear documentation of the <b>work processes and tasks</b> according to control loops or <b>action&shy;regulation&shy;cycles</b> (PDCA). According to certain criteria, they are to be kept as part of the quality manual. Using the process overview, employees can view responsibilities and duties at any time, request them if necessary and familiarize themselves better with the processes and work steps assigned as tasks.</p>
	<h2>Process responsibility</h2>
	<p>The standard expects the determination of a person responsible for the process. This is responsible for reconciling them. She is also to be addressed if there are bottlenecks or process problems. The validity date or status of the determination must also be documented.</p>
	<h2>Creating the process overview</h2>
	<p>In the overview, the individual views are to be created for each company or organizational area. The processes assigned to the area are to be listed and subdivided hierarchically.</p>
	<p>Furthermore, those responsible for each sub-process or elementary process must be named in accordance with the organizational structure (organizational chart). References to other applicable documents, in particular work instructions and forms, may be entered as hyperlinks.</p>
	<p>Each process/work step should be described as clearly and comprehensibly as possible:</p>
	<ul>
	<li><b>Function/task</b> of the respective process/work step with <b>PDCA cycle part</b> and the <b>organizational unit</b> responsible for it (area, department) from the organization chart. The last one is then the providing or receiving unit for the following input and output data.</li>
	<li><b>Incoming data/documents</b> with the organizational unit providing the service.</li>
	<li><b>Output data/documents</b> with receiving organizational unit.</li>
	<li>Necessary additional information such as <b>Applicable documents, info</b> etc. These can also be links to such documents. It depends very much on the work task and the hierarchical level whether such documents can be assigned or not.</li>
	</ul>
	<h2>Hierarchical description (granulation)</h2>
	The <b>process steps</b> or <b>work tasks</b> are sorted based on their time requirements and attention is paid to <b>complete cycles</b> in each case. A level's time allowance is <b>the sum</b> of those below it.</p>
	<p>On the process level, the term <b>'management'</b> can make sense, e.g. <i>HR-management</i> can be used as a key term. Otherwise, so-called <b>function sentences</b> (object-verb combination), e.g. <i>Create employee list</i> can be used for elementary process and work step level.</p>
	<p>When formulating the elementary processes and work steps, a meaningful and standardized formulation is to be used. This not only makes the description easier, but also makes the steps easier for employees to understand. The following table makes a rough suggestion.</p>
	<table style="width:100%;">
	<tr><td colspan=4 style="font-weight:bold;">Naming standards for elementary process and work step level</td></tr>
	<tr><td style="width:22%">Process cycle [ISO 9001/14001]</td><td style="width:14%">Full</td><td style="width:22% ">Partial steps</td><td style="width:42%">Individual steps</td></tr>
	<tr><td>P: plan [plan]</td><td class="tabCol" rowspan=4>handle, ensure, maintain, publish</td><td class="tabCol" rowspan=2>create , define, execute</td><td class="tabCol">plan, coordinate</td></tr>
	<tr><td>D: carry out [do]</td><td class="tabCol">process, commission, derive, implement</td></tr>
	<tr><td>C: check [check]</td><td class="tabCol" rowspan=2>manage, release, readjust, validate</td><td class="tabCol">(over-) check, track, verify, monitor</td></tr>
	<tr><td>A: re-act [act]</td><td class="tabCol">revise, decide, correct</td></tr>
	</table>`;

	useEngineer.lockOpener = `With <b>Fixing mode</b> enabled, the current state of the file is immutable. This is used, for example, for versioning.<br>
	The content can no longer be edited, if work data saved locally as an HTML file in fix mode by using the button: &#x1F4CB;. Later the header functions (icons) are <b>no longer available</b> in the created file.<br>
	The <b>Checkbox</b> is primarily used for control when saving. <b>Currently</b> the fix mode has <b>only the effect</b> of preventing changes and thus simulating the later behavior after saving.`;

	useEngineer.emptyView = [
	{ "act": "columnCopy", "data": "info" }
	, { "act": "columnHeader", "data": "info" }

	, { "act": "columnCopy", "data": "top" }
	, { "act": "columnSize", "data": "p40" }
	, { "act": "columnHeader", "data": "Process responsible" }
	, { "act": "typeCopy", "data": "boxtext" }	

	, { "act": "columnCopy", "data": "top" }
	, { "act": "columnSize", "data": "p10" }
	, { "act": "columnHeader", "data": "Org. unit" }
	, { "act": "typeCopy", "data": "boxtext" }	

	, { "act": "columnCopy", "data": "top" }
	, { "act": "columnSize", "data": "p30" }
	, { "act": "columnHeader", "data": "available" }
	, { "act": "typeCopy", "data": "boxtext" }	

	, { "act": "columnCopy", "data": "top" }
	, { "act": "columnSize", "data": "p20" }
	, { "act": "columnHeader", "data": "Validity date (state)" }
	, { "act": "typeCopy", "data": "boxtext" }	
	, { "act": "typeEdit", "data": ( new Date ).toLocaleDateString() }	

	, { "act": "columnCopy", "data": "fullrow" }
	];

	useEngineer.summary = [
	{ "act": "columnCopy", "data": "overview" }
	, { "act": "columnHeader", "data": "overview" }
	];

(function( monitor ) {
// monitors if a unit is defined as responsible for some thing. if a inputUnit or
// outputUnit is filled in and this unit is not given with responsibility it will be
// marked as a hint to consider it.
var
	uE = useEngineer
	;
	monitor.forEach( a => uE.calc[ a ] = function( acts, act, type ) {
	var
		list = { lead: [], view: {}}
		, count = 0
		;
		_search( act.view );
//pAlert(monitor,type,JSON.stringify( list,null,'  '), JSON.stringify( act,null,'  '));
		if ( type == monitor[ 0 ] ) {
			for ( var id in list.view )	// set every unitInput and unitOutput of view
				_set( id, list.view[ id ]);
		}
		else
			_set( act.id, act.data ); // check and set active field only
			uE.$CN[ !count ? 'remove' : 'add' ]( document.body.querySelector( 'div.grow' )
			, 'warning' );

		function _search( setView ) {
		// get all leading units and all unitInput and unitOutput of setView also
		var
			idL = []
			, leadL = []
			;
			for ( var n in acts ) {
				if ( n != 'view-view') {
					acts[ n ].forEach( function( a ) {
					var
						f = a.act == 'typeCopy' && monitor.indexOf( a.data.slice( 5 )) > -1
						;
						if ( f && a.data.slice( 5 ) == monitor[ 0 ]) {
							idL.push( a.id );
							leadL.push( a.id );
						}
						else if ( f && n == setView )
							idL.push( a.id );
					});
					acts[ n ].forEach( function( a ) {
						if ( a.act == 'typeEdit' && idL.indexOf( a.id ) > -1 ) {
							if ( leadL.indexOf( a.id ) > -1 )
								list.lead[ a.id ] = a.data.trim();
							else
								list.view[ a.id ] = a.data.trim();
						}
					});
				}
			}				
			list.lead = Object.values( list.lead ).filter(function( v, p, self ) {
 				return v.length && self.indexOf( v ) == p;	// set unique
			});
		}
		function _set( id, data ) {
		var
			action = list.lead.indexOf( data ) > -1 || data == '' ? 'remove' : 'add'
			;
			uE.$CN[ action ](	uE.$( id ).querySelector( '.content' ), 'warning' );
			if ( action == 'add' )
				count++;
		}
	});
})([ 'unit', 'unitInput', 'unitOutput' ]);

	useEngineer.getExport = function( acts, activeTool, title, types, steps ) {
	var
		viewNames = {}
		, jsTemplate = {}
		, jsFind = {}
		, defTask = false
		, json = {}
		;
		for ( var i = 0, t = activeTool.types, tt, st; i < t.length; i++ ) {
			tt = types[ t[ i ]];
			st = !tt.step1 ? false : steps[ tt.step1[ 0 ]];
			if ( t[ i ] == "task" ) {
				for ( var j = 0, n; j < st.descript.length; j++ ) {
					n = st.descript[ j ].replace( /\s*\([^\)]+\)/, '' );
					if ( j == tt.step1[ 1 ])
						defTask = n;
					jsFind[ st.class[ j ]] = n;
					jsTemplate[ n ] = '';					
				}
			}
			else {
				jsFind[ t[ i ]] = tt.title;
				jsTemplate[ tt.title ] = !st ? ""	// replace start string of class
				: st.class[ tt.step1[ 1 ]].replace( tt.step1[ 0 ], '' );
			}
		}
		jsTemplate = JSON.stringify( jsTemplate );
		acts[ 'view-view' ].filter( a => a.act == "viewEdit"
		).forEach( a => viewNames[ a.id ] = a.data );
//pAlert(Object.keys(acts),JSON.stringify( viewNames,null,'  '));
		for ( var n in acts )
			if ( n.indexOf( 'view-' ) == -1 )
				json[ viewNames[ n ]] = _analyse( acts[ n ]);
//pAlert(Object.keys(json),JSON.stringify( json,null,'  '));
		return json;

		function _analyse( a ) {
		var
			order = a.filter( a => a.act == "typeCopy" ).map( a => a.id )
			, type = {}
			, _json = []
			;
			a.filter( a => a.act == "typeCopy" ).forEach( a =>
				type[ a.id ] = { type: a.data.slice( 5 ) || '' }
			);
			a.filter( a => a.act == "typeEdit" ).forEach( a =>
				type[ a.id ].text = a.data
			);
			a.filter( a => a.act == "ripe" ).forEach( a =>
				type[ a.id ].data = a.data.slice( 4 )
			);
			a.filter( a => a.act == "cycle" ).forEach( a =>
				type[ a.id ].data = a.data
			);
			a.filter( a => a.act == "task" ).forEach( a =>
				type[ a.id ].data = a.data
			);
			for ( var i = 0, j = 0, t; i < order.length; i++ ) {
				if ( order[ i ].length > 1 ) {
					if ( t = type[ order[ i ]]) {
						if ( t.type == 'task' ) {
							_json.push( JSON.parse( jsTemplate ));
							j = _json.length -1;
							if ( t.text )
								_json[ j ][ t.data ? jsFind[ t.data ] : defTask ] = t.text;
						}
						else if ( t.data )
							_json[ j ][ jsFind[ t.type ]] = t.data;
					}
				}
			}
//pAlert(viewNames[ n ],JSON.stringify( type,null,'  '),JSON.stringify( _json,null,'  '));
			return _json;
		}
	};
	useEngineer.getImport = function( myId, a, types, subTypes ) {
		if ( Array.isArray( a )) {
			for ( var i = 0, m = Array.isArray( a[ 0 ]), js = [], last = 'column-none'
			; i < a.length; i++ ) {
				js.push( _get( 'columnCopy', "fullrow", i +100 ));
				js.push( _get( 'columnMove', last, i +100 ));
				last = myId +'-'+ ( i +100 );

				js.push( _get( 'typeCopy', 'task', i +5100 ));
				js.push( _get( 'typeMove', last, i +5100 ));
				js.push( _get( 'typeEdit', m ? a[ i ][ 0 ] : a[ i ], i +5100 ));
				js.push( _get( 'typeCopy', 'cycle', i +10100 ));
				js.push( _get( 'typeMove', myId +'-'+ ( i +5100 ), i +10100 ));
				if ( m && a[ i ][ 1 ])
					js.push( _get( 'typeEdit', a[ i ][ 1 ], i +10100 ));
				js.push( _get( 'typeCopy', 'unit', i +15100 ));
				js.push( _get( 'typeMove', myId +'-'+ ( i +10100 ), i +15100 ));
				if ( m && a[ i ][ 2 ])
					js.push( _get( 'typeEdit', a[ i ][ 2 ], i +15100 ));
				js.push( _get( 'typeCopy', 'noteInput', i +20100 ));
				js.push( _get( 'typeMove', myId +'-'+ ( i +15100 ), i +20100 ));
				if ( m && a[ i ][ 3 ])
					js.push( _get( 'typeEdit', a[ i ][ 3 ], i +20100 ));
				js.push( _get( 'typeCopy', 'unitInput', i +25100 ));
				js.push( _get( 'typeMove', myId +'-'+ ( i +20100 ), i +25100 ));
				if ( m && a[ i ][ 4 ])
					js.push( _get( 'typeEdit', a[ i ][ 4 ], i +25100 ));
				js.push( _get( 'typeCopy', 'noteOutput', i +30100 ));
				js.push( _get( 'typeMove', myId +'-'+ ( i +25100 ), i +30100 ));
				if ( m && a[ i ][ 5 ])
					js.push( _get( 'typeEdit', a[ i ][ 5 ], i +30100 ));
				js.push( _get( 'typeCopy', 'unitOutput', i +35100 ));
				js.push( _get( 'typeMove', myId +'-'+ ( i +30100 ), i +35100 ));
				if ( m && a[ i ][ 6 ])
					js.push( _get( 'typeEdit', a[ i ][ 6 ], i +35100 ));
				js.push( _get( 'typeCopy', 'remark', i +40100 ));
				js.push( _get( 'typeMove', myId +'-'+ ( i +35100 ), i +40100 ));
				if ( m && a[ i ][ 7 ])
					js.push( _get( 'typeEdit', a[ i ][ 7 ], i +40100 ));
			}
			return JSON.parse( '['+ js.join( ',' ) +']' );
		}
		return [];

		function _get( a, d, id ) {
			return '{"act":"'+ a +'","data":"'+ d +'","id": "'+ myId +'-'+ id +'"}';
		}
	};

	function init() {
	   useEngineer.init([46191,2135,38949,46269,36648,58393,65415,27163,46603,55789,42928,43021,11457,55259,5051,8714,51942,41838,33906,46823,29838,3671,53649,4312,25394,47931,47517,49826,12868,46464,2037,3982,9536,40926,50715,5613]);
	}
	//alert( useEngineer.getInitcode());

</script>

<link id="useEngineerStyle" rel="Stylesheet" href="useEngineer.css" type="text/css">
<style id="localStyle" type="text/css">

body.touch #source, body.touch div.views {
	display: none;
}
body.touch div.table { width: 100%; }

body.lockedMode #source
, body.lockedMode #trash
, body.lockedMode #head
, body.lockedMode .column div.size
, body.lockedMode div.column.info
, body.testing .column div.size {
	display: none;
}
body.testing #source
, body.testing #trash
, body.testing #head {
	opacity: 0.2;
}
#source div.column {
	font-size: 0.7em;
}
#source #type-task .content {
	font-size: 1.4em;
	font-style: italic;
}

div.type {
	display: table;
	height: 2.9em;
	border-style: solid;
	border-width: 0 0.1em 0.1em 0;
}
div.column {
	width: 100%;
	height: 2.9em;
	border-width: 1px;
	border-top-left-radius: 0.2em;
	border-bottom-left-radius: 0.2em;
}
div.column.info, div.column.calc {
	height: auto;
	min-height: 1.5em;
	font-size: 0.7em;
	padding: 0.3em 0.3em;
	border-radius: 0;
}
div.column.info p {
	font-size: 0.8em;
	width: 33%;
	float: left;
	padding-left: 0.6em;
	line-height: 1.3em;
}
div.column.top {
	height: auto;
	min-height: 1.2em;
	font-size: 0.7em;
	padding: 0.3em 0 0.3em 0.6em;
}
div.column.calc {
	text-align: right;
}
div.column.off {
	height: 0.8em!important;
	overflow: hidden;
}
div.column.off p { display:none; }
.column div.size {
	position: relative;
	float: left;
	display: block;
	width: 1.0em;
	height: 2.8em;
	font-size: 1.0em;
	margin-right: 0.3em;
	background-image: radial-gradient(#9e9e9e 1px, transparent 1px);
	background-size: 4px 4px;
	background-color: #dddddd;
	border: 0.1em outset #bbbbbb;
	border-radius: 0.2em;
}

div.columnHeader {
	font-size: 0.8em;
	line-height: 1.0em;
	margin-bottom: 0.1em;
}
.type.boxtext {
	position: relative;
	height: 1.7em;
	width: 100%;
	border: none;
}

.type.boxtext > div.content {
	text-align: left;
	font-size: 1.0em;
	width: 100%;
	height: 100%;
	margin: 0;
	padding: 0;
}

div.column.overview {
	height: auto;
	padding: 0.8em;
}
div.column.overview * {
	font-size: 0.9em;
	line-height: 1.3em;
}

div.column.overview table td {
	border: 1px solid #999999;
	text-align: left;
	padding: 0.5em;
}

div.column.overview table .tabCol {
	background-color: #eeeeee;
}

div.view:after
, div.noteInput:after
, div.noteOutput:after
, div.remark:after
, div.column.off:after
, div.cycle div.cycle:before
, div.cycle div.cycle:after
, div.type.cycle:hover:after
, div.type > div.content
, .type.unit:hover:after
, .type.unitInput:hover:after
, .type.unitOutput:hover:after
, .type.process:after
, .type.keyTask:after
, .type.mainTask:after
, .type.subTask:after
, .type.action:after {
	position: absolute;
	left: 0;
	height: auto;
	font-size: 0.7em;
	line-height: 1.2em;
	text-align: left;
	vertical-align: top;
	display: inline-block;
}
div.view:after {
	content: attr(data);
	bottom: 0;
	width: 100%;
	text-align: center;
	color: #444444;
	background-color: rgba( 200, 200, 200, 0.5 );
	margin: 0;
	padding: 0.1em 0;
}
div.view.active {
	border: 2px solid #767686;
}
div.view.active:after {
	animation: backcolor 4.0s infinite;
}
div.view img {
	width: 125%;
}
div.column.info.off:after {
	content: "\2139\FE0F";
	font-size: 1.0em;
	top: 0;
}

div.type > div.content {
	top: 0;
	padding: 0 0.4em;
	z-index: 1;
	overflow: hidden;
}
div.type.unit,
div.type.unitInput,
div.type.unitOutput,
div.type.unit > div.content,
div.type.unitInput > div.content,
div.type.unitOutput > div.content {
	text-align: left!important;
}

div.type.unitInput > div.unitSelect,
div.type.unitOutput > div.unitSelect,
div.type > div.cycle,
div.type > div.task {
	display: table-cell;
   width: 100%;
	height: 100%;
	padding: 0;
}
div.type > div.task {
	background-color: #b3c9fc;
}
div.type.process > div.task {
	background-image: repeating-linear-gradient(0deg, #4386F5, #4386F5 1px
		, transparent 1px, transparent);
	background-size: 40px 4px;
}

div.noteInput, div.noteOutput {
	border-right-style: dashed;
}
div.noteInput:after
, div.noteOutput:after
, div.remark:after
, div.type.cycle:hover:after 
, .type.unit:hover:after
, .type.unitInput:hover:after
, .type.unitOutput:hover:after
, .type.process:after
, .type.keyTask:after
, .type.mainTask:after
, .type.subTask:after
, .type.action:after {
	content: attr(title);
	font-size: 0.5em;
	color: #333333;
	left: 0;
	top: 0;
	padding: 0.1em 0.6em;
	background-color: #ffffff;
	opacity: 0.8;
	border-radius: 0.4em;
	z-index: 4;
}

.type.unit:hover:after
, .type.unitInput:hover:after
, .type.unitOutput:hover:after {
	border-top: 0.1em solid #333333;
	border-right: 0.1em solid #333333;
}
div.type.cycle:hover:after 
, .type.process:after
, .type.keyTask:after
, .type.mainTask:after
, .type.subTask:after
, .type.action:after {
	content: attr(data);
	left: 0.8em;
	top: auto!important;
	bottom: 0.3em;
	letter-spacing: normal;
	white-space: nowrap;
}
.type.noteInput:after
, .type.noteOutput:after
, .type.remark:after {
	z-index: 3;
}

div.type.cycle {
	display: table;
	font-size: 1.0em;
	line-height: 1.3em;
	font-family:'Lucida Console', Monaco, monospace; 
	letter-spacing: 0.3em;
	padding: 0.25em 0 0 0.35em;
	overflow: hidden;
}
div.cycle div.cycle:before {
	content: '';
	top: 0;
	width: 100%;
	height: 50%;
	border-bottom: 1px solid #cccccc;
}
div.cycle div.cycle:after {
	content: '';
	top: 0;
	width: 50%;
	height: 100%;
	border-right: 1px solid #cccccc;
}

div.type > div.unitSelect {
	font-size: 1.2em;
	padding: 0 0 0.7em 0;
	text-align: center;
   vertical-align: bottom;
   height: 100%!important;
}

.type.process, .type.keyTask, .type.mainTask, .type.subTask, .type.action {
	width: calc(37% - 3.9em);
}
.type.noteInput, .type.noteOutput, .type.remark {
	width: 16%;
}
.type.unit, .type.unitInput, .type.unitOutput {
	width: 5%;
}
div.type.cycle {
	width: 2.0em;
	border-right-width: 0.2em;
	overflow: hidden;
}

.type.unit > div.content,
.type.unitInput > div.content,
.type.unitOutput > div.content {
	height: 100%;
	width: 100%;
	margin: 0;
	padding: 1.2em 0.4em 0.4em;
	word-break: break-all;
}
.type.noteInput > div.content,
.type.noteOutput > div.content,
.type.remark > div.content	{
	width: 100%;
	height: inherit;
	overflow-y: auto;
	overflow-x: hidden;
	margin: 0;
	padding: 1.2em 0.4em 0.4em;
	word-break: break-all;
}
.type.unitInput > div.content.warning,
.type.unitOutput > div.content.warning {
	background-color: yellow;
	font-style: italic;
}
div.grow.warning:after {
	position: absolute;
	font-size: 0.6em;
	line-height: 0.8em;
	top: 7.5em;
	right: 5.0em;
	padding: 0.5em 0.8em;
	background-color: yellow;
	border-radius: 0.5em;
}
div.grow.warning:lang(de):after {
	content: "noch keine Zuständigkeit definiert";
}
div.grow.warning:lang(en):after {
	content: "no responsibility defined yet";
}

.type.process > div.content,
.type.keyTask > div.content,
.type.mainTask > div.content,
.type.subTask > div.content,
.type.action > div.content {
	height: 100%;
	width: 76%;
	margin: 0;
	padding: 0.2em 0.4em;
	background-color: #ffffff;
	border: 0.2em inset #bbbbbb;
}
.type.process > div.content {
	margin-left: 0;
	margin-right: 24%;
}
.type.keyTask > div.content {
	margin-left: 6%;
	margin-right: 18%;
}
.type.mainTask > div.content {
	margin-left: 12%;
	margin-right: 12%;
}
.type.subTask > div.content {
	margin-left: 18%;
	margin-right: 6%;
}
.type.action > div.content {
	margin-left: 24%;
	margin-right: 0;
}

div.column.info.on:after {
	content: '\1F197';
	position: absolute;
	top: 0.2em;
	right: 0.2em;
}
div.column.info.on:after {
	top: auto;
	bottom: 0.1em;
}

div.info > div.onoff {
	position: absolute;
	left: 0;
	top: 0;
   width: 100%;
	height: 100%;
	padding: 0;
}

.p50 { 
	min-width: 50%;
	max-width: 50%;
}
.p40 { 
	min-width: 40%;
	max-width: 40%;
}
.p30 { 
	min-width: 30%;
	max-width: 30%;
}
.p25 { 
	min-width: 25%;
	max-width: 25%;
}
.p20 { 
	min-width: 20%;
	max-width: 20%;
}
.p10 { 
	min-width: 10%;
	max-width: 10%;
}

</style>
</head>

<body class="notActive" onload="init()" ondragover="return useEngineer.allow(event)"
	ondrop="useEngineer.dropfile(event)">
	<div class="fixed">
		<div class="banner" role="banner">
			<div id="logo"></div>
			<h1 id="title"></h1>
			<div id="copyright"><a href="http://www.use-optimierung.de" target="_blank"
			>© designed and developed by Dr. Dirk Fischer, Cologne (version 1.0)</a><br
			><button id="recommend">&#x1F44D;</button
			><button id="initAccount">&#x1F464;</button
			><button id="addGroup">&#x1F465;</button
			><button id="numberList">&#x1F511;</button
		></div
		></div
		><div id="head">
			<button id="showHelp">&#x2753;</button
			><button id="addView">&#x1F195;</button
			><button id="zoomin">&#x1F50D;</button
			><button id="zoomout">&#x1F50E;</button
			><button id="appearance">&#x2003;</button
			><button id="storeJSON">&#x1F4BE;</button
			><button id="storeHTML">&#x1F4CB;</button
			><button id="export">&#x1F4DD;</button
			><button id="privatework"></button
			><input id="copytoggle" type="checkbox"><label for="copytoggle" id="copyLabel"></label
			><input id="locktoggle" type="checkbox"><label for="locktoggle" id="lockLabel"></label
			><div id="groupMode">&#x1F310;</div
		></div
		><div><div id="trash"></div><div id="source" ></div></div
	></div
	><div class="grow"
		><div class="views"><div id="views"></div></div
		><div class="table"><div id="table"></div></div
	></div>
	<div id="protocol" role="main"><h1 id="h1"></h1></div>

<noscript><p>Diese Datei benötigt aktiviertes JavaScript! Als E-Mail-Anhang versendet, muss diese erst gespeichert werden, damit sie funktioniert.</p>
<p>This file requires JavaScript enabled! Sent as an email attachment, it must first be saved for it to work.</p>
</noscript>
<!--useAsApp /* dies durch JSON-Inhalt ersetzen ** replace this with JSON content */ -->
</body>
</html>